﻿using UnityEngine;

public class TapToTransform : MonoBehaviour
{
    public Sprite toTransform;
    private Vector2 roundedPos;
    private int counter = 0;
    private Camera mainCamera;
    
    void Start()
    {
        mainCamera = CameraInstance.Instance.GetComponent<Camera>();
        roundedPos = Utils.RoundPosition(transform.position);
    }
    
    void Update()
    {
        if (Input.touchCount == 1)
        {
            Touch touch = Input.GetTouch(0);

            Vector2 cameraPos = mainCamera.ScreenToWorldPoint(Input.GetTouch(0).position);
            Vector2 roundedPos = Utils.RoundPosition(cameraPos);

            if (roundedPos == this.roundedPos)
            {
                if (touch.phase == TouchPhase.Began)
                {
                    counter++;
                    if (counter > 10)
                    {
                        this.gameObject.GetComponent<SpriteRenderer>().sprite = toTransform;
                        Destroy(this);
                    }
                }
            }
        }
    }
}
