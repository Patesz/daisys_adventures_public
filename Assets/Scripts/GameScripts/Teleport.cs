﻿using Character;
using UnityEngine;

namespace GameComponent
{
    public class Teleport : MonoBehaviour
    {
        public bool teleported { get; set; }

        private void Start()
        {
            Player.Instance.wrongLocations.Add(Utils.RoundPosition(transform.position));
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (Player.go)
            {
                if (collision.name == "Character")
                {
                    teleported = true;
                }
            }
        }

    }
}

