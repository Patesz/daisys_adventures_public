﻿using TMPro;
using UnityEngine;

namespace Character
{
    public class DirectionPanel : MonoBehaviour
    {
        private Animator anim;

        public bool isDirectionSet { get; set; }
        public bool isDaisyClicked { get; set; }
        private bool breakMouseDrag = false;

        private Camera mainCamera;
        private Player playerScript;

        private GameObject directionStatus;
        private Animator dirAnim;
        private TextMeshProUGUI dirText;

        // 0: up, 1: down, 2: left, 3: right
        private readonly Transform[] arrows = new Transform[4];
        private readonly SpriteRenderer[] arrowPanels = new SpriteRenderer[4];
        private readonly Vector2[] arrowRoundedPos = new Vector2[4];

        private Transform character;
        private BoxCollider2D characterBoxCollider;
        private Vector2 originalSize = new Vector2();

        #region Start
        private void Start()
        {
            isDirectionSet = false;
            isDaisyClicked = false;

            ArrowList();
            ArrowPanelList();

            directionStatus = GameObject.Find("DirectionStatus");
            dirAnim = directionStatus.GetComponent<Animator>();
            dirText = directionStatus.transform.Find("DirectionText").GetComponent<TextMeshProUGUI>();

            mainCamera = CameraInstance.Instance.GetComponent<Camera>();

            character = this.transform.Find("Character");
            anim = character.GetComponent<Animator>();
            characterBoxCollider = character.GetComponent<BoxCollider2D>();

            playerScript = this.GetComponent<Player>();

            ArrowRoundedPositions();
            originalSize = characterBoxCollider.size;
            SetAllArrowActive(false);
        }

        private void ArrowList()
        {
            for (int i = 0; i < 4; i++)
            {
                arrows[i] = transform.Find("DirectionPanelSelector/" + Utils.dir[i] + "Arrow");
            }
        }

        private void ArrowPanelList()
        {
            for (int i = 0; i < 4; i++)
            {
                arrowPanels[i] = arrows[i].Find(Utils.dir[i] + "ArrowPanel").GetComponent<SpriteRenderer>();
                arrowPanels[i].enabled = false;
            }
        }

        private void ArrowRoundedPositions()
        {
            for (int i = 0; i < 4; i++)
            {
                arrowRoundedPos[i] = Utils.RoundPosition(arrows[i].transform.position);
            }
        }

        #endregion

        #region MouseActions
        private void OnMouseDown()
        {
            SetDirectionPanel(true, new Vector2(4f, 4f));
        }

        private void OnMouseDrag()
        {
            if (!breakMouseDrag)
            {
                Vector2 cameraPos = mainCamera.ScreenToWorldPoint(Input.mousePosition);
                Vector2 roundedPos = Utils.RoundPosition(cameraPos);

                for (int i = 0; i < 4; i++)
                {
                    if (roundedPos == arrowRoundedPos[i])
                    {
                        SetArrowPanelActive(i, true);
                    }
                    else
                    {
                        SetArrowPanelActive(i, false);
                    }
                }
            }
        }
        private void OnMouseUpAsButton()
        {
            Debug.Log("OnMouseUpOverGO");
            if (!breakMouseDrag)          // on touch it is absolutely unnecesarry to handle breakmousedrag
            {
                breakMouseDrag = true;
                Vector2 cameraPos = mainCamera.ScreenToWorldPoint(Input.mousePosition);
                Vector2 roundedPos = Utils.RoundPosition(cameraPos);

                if (roundedPos == arrowRoundedPos[0])
                {
                    playerScript.setMoveUpTrue();           // it determines which way the character will go when you start the game
                    DirectionSet("GoIdleUp", "GoIdle", "Up", true);
                }
                else if (roundedPos == arrowRoundedPos[1])
                {
                    playerScript.setMoveDownTrue();
                    DirectionSet("GoIdle", "GoIdleUp", "Down", true);
                }
                else if (roundedPos == arrowRoundedPos[2])
                {
                    playerScript.setMoveLeftTrue();
                    if (character.transform.rotation.y != 0)
                    {                        // character rotates to selected direction
                        character.transform.eulerAngles = new Vector3(0, 0, 0);
                    }
                    DirectionSet("GoIdle", "GoIdleUp", "Left", true);
                }
                else if (roundedPos == arrowRoundedPos[3])
                {
                    playerScript.setMoveRightTrue();
                    if (character.transform.rotation.y != 180)
                    {
                        character.transform.eulerAngles = new Vector3(0, 180, 0);
                    }
                    DirectionSet("GoIdle", "GoIdleUp", "Right", true);
                }
                else
                {
                    Debug.Log("Nothing selected");
                    isDirectionSet = false;
                }

                if (isDirectionSet)
                {
                    SetAllArrowActive(false);
                    SetArrowPanelActive(false);
                    dirAnim.SetTrigger("Show");
                    characterBoxCollider.size = originalSize;
                }
            }
        }

        private void OnMouseExit()
        {
            if (!breakMouseDrag)          // on touch it is absolutely unnecesarry to handle breakmousedrag
            {
                Debug.Log("OnMouseExit");

                Vector2 cameraPos = mainCamera.ScreenToWorldPoint(Input.mousePosition);
                Vector2 roundedPos = Utils.RoundPosition(cameraPos);

                if (roundedPos == arrowRoundedPos[0])
                {
                    playerScript.setMoveUpTrue();           // it determines which way the character will go when you start the game
                    DirectionSet("GoIdleUp", "GoIdle", "Up", true);
                }
                else if (roundedPos == arrowRoundedPos[1])
                {
                    playerScript.setMoveDownTrue();
                    DirectionSet("GoIdle", "GoIdleUp", "Down", true);
                }
                else if (roundedPos == arrowRoundedPos[2])
                {
                    playerScript.setMoveLeftTrue();
                    if (character.transform.rotation.y != 0)
                    {                        // character rotates to selected direction
                        character.transform.eulerAngles = new Vector3(0, 0, 0);
                    }
                    DirectionSet("GoIdle", "GoIdleUp", "Left", true);
                }
                else if (roundedPos == arrowRoundedPos[3])
                {
                    playerScript.setMoveRightTrue();
                    if (character.transform.rotation.y != 180)
                    {
                        character.transform.eulerAngles = new Vector3(0, 180, 0);
                    }
                    DirectionSet("GoIdle", "GoIdleUp", "Right", true);
                }
                else
                {
                    Debug.Log("Nothing selected");
                    isDirectionSet = false;
                }

                if (isDirectionSet)
                {
                    SetAllArrowActive(false);
                    SetArrowPanelActive(false);
                    dirAnim.SetTrigger("Show");
                }
            }
            SetDirectionPanel(false, originalSize);
            SetArrowPanelActive(false);
        }

        private void Update()
        {
            if (Player.go)
            {
                Destroy(GameObject.Find("DirectionPanelSelector"));
                Destroy(this);
            }
        }
        #endregion

        #region DirectionPanelSelector
        private void SetDirectionPanel(bool isTrue, Vector2 size)
        {
            isDaisyClicked = isTrue;
            SetAllArrowActive(isTrue);
            characterBoxCollider.size = size;
            breakMouseDrag = !isTrue;
        }

        private void DirectionSet(string activateAnim, string deactiveAnim, string direction, bool directionSet)
        {
            anim.SetBool(activateAnim, true);
            anim.SetBool(deactiveAnim, false);
            dirText.text = "Start direction: " + direction;
            isDirectionSet = directionSet;
        }


        private void SetAllArrowActive(bool isActive)
        {
            for (int i = 0; i < arrows.Length; i++)
            {
                arrows[i].gameObject.SetActive(isActive);
            }
        }

        private void SetArrowPanelActive(int index, bool isActive)
        {
            arrowPanels[index].enabled = isActive;
        }

        private void SetArrowPanelActive(bool isActive)
        {
            for (int i = 0; i < arrowPanels.Length; i++)
            {
                arrowPanels[i].enabled = isActive;
            }
        }
        #endregion

    }
}
