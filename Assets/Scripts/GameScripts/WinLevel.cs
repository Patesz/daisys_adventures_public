﻿using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using Character;
using Arrow;

public class WinLevel : MonoBehaviour {
    private readonly Animator starAnim;

    private SpriteRenderer cofferSpriteRenderer;
    private Vector2 winPanelPos;
    private static string currScene;
    private static int topScore;
    private GameObject [] star;     // DO NOT USE IT AS A STATIC VARIABLE !!!

    //public Sprite treasureClosed_img;
    public Sprite treasureOpened_img;

    private void initialize(){
        star = GameObject.FindGameObjectsWithTag("Star");

        cofferSpriteRenderer = GameObject.Find("Coffer").GetComponent<SpriteRenderer>();
        cofferSpriteRenderer.sprite = treasureOpened_img;
        GameObject.Find("Coffer/TreasureParticleEffect").GetComponent<ParticleSystem>().Play();
        
        currScene = SceneManager.GetActiveScene().name;          // get current scene name
        //nextScene = nextLevelName(currScene);                  // get next scene name using nextLevelName function
        topScore = PlayerPrefs.GetInt(currScene,0);              // get new or already earned stars number
        Debug.Log("Currscene: "+currScene);
        Debug.Log("Top score: "+topScore);
    }

    private void Start(){
        if(Time.timeScale != 1f)
            Time.timeScale = 1f;
        
        this.transform.position = new Vector2(-1000f,0f);
    }

    public void WinThisLevel()
    {
        if(Time.timeScale != 1f)
            Time.timeScale = 1f;

        initialize();

        int points = Player.Instance.Points;
        if (topScore < points)
        {                                               // if current best is worst then the new score, then update it
            PlayerPrefs.SetInt(currScene, points);            // stores each scene earned stars value (how many stars to unlock)
        }
        this.transform.position = new Vector2(0f,0f);
        UnlockLevel();
        ArrowManager.Instance.DestroyArrowScripts();
        DisableDaisy();
        DestroySettingsBtn();
        DestroyStartDaisyBtn();
        DestroyMagnifyGlass();
        DestroyWrongPosPrefab();
        
        if (points <= 3)
        {
            GetResults(points);
        }
    }

    private void UnlockLevel(){
        int levelNum = GameManagerScript.Instance.getLevelNumFromLevelName(SceneManager.GetActiveScene().name);

        int beatenLevels = PlayerPrefs.GetInt("levelReached");
        beatenLevels++;

        bool isNewLevelUnlocked = false;
        int sumStars = CountStars(beatenLevels);

        if( levelNum == beatenLevels ){
            isNewLevelUnlocked = true;

            if(beatenLevels % 10 == 0 && sumStars < beatenLevels * 1.5f){
                Debug.Log("You beat this section of the game hell yeah man, but you only have " +
                 sumStars + " score, and you need at least " + beatenLevels * 1.5f);
                return;
            }
            else{
                PlayerPrefs.SetInt("levelReached", beatenLevels);
            }
        }
        if(!isNewLevelUnlocked && beatenLevels % 10 == 0 && sumStars > beatenLevels * 1.5f){
            Debug.Log("You successfully unlocked a new stage. :D");
            PlayerPrefs.SetInt("levelReached", beatenLevels);
        }

    }

    private int CountStars(int levelBeaten){
        int sumStars = 0;
        for(int i=0; i<levelBeaten; i++){
            sumStars += PlayerPrefs.GetInt("Level" + (i+1));
        }
        return sumStars;
    }

    public void DestroyMagnifyGlass(){
        Destroy(GameObject.Find("GameContainer/MagnifyGlass"));
    }
    public void DestroyWrongPosPrefab(){
        Destroy(GameObject.Find("GameContainer/WrongPositionPrefab"));
    }
    private void DestroySettingsBtn(){
        Destroy(GameObject.Find("Pause_btn"));
    }
    private void DestroyStartDaisyBtn()
    {
        Destroy(GameObject.Find("StartDaisy_btn"));
    }

    private void GetResults(int points){
        if(points == 0){
            for (int i = 0; i < star.Length; i++){
                star[i].SetActive(false);
            }
            TextResult("You can do better than that!");
        }
        else if(points == 1){
            for (int i = 0; i < star.Length; i++){
                if(star[i].name == "star1_img"){
                    star[i].GetComponent<Animator>().Play("star1");
                }
                else{
                    star[i].SetActive(false);
                }
                TextResult("Not bad");
            }
        }
        else if(points == 2){
            for (int i = 0; i < star.Length; i++){
                if(star[i].name == "star3_img"){
                    star[i].SetActive(false);
                }
                else{
                    star[i].GetComponent<Animator>().Play("star" + (i+1));
                }
                TextResult("Nice job.");
            }
        }
        else{
            for (int i = 0; i < star.Length; i++){
                star[i].GetComponent<Animator>().Play("star" + (i+1));
            }
            TextResult("Excellent job!");
        }
    }

    private void TextResult(string result){
        TextMeshProUGUI result_text = transform.Find("result").GetComponent<TextMeshProUGUI>();
        result_text.text = result;
    }

    private void DisableDaisy(){
        GameObject.Find("Character").GetComponent<BoxCollider2D>().enabled = false;
    }
    
}