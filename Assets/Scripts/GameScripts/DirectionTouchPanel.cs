﻿using System.Collections;
using TMPro;
using UnityEngine;

namespace Character
{
    public class DirectionTouchPanel : MonoBehaviour
    {
        private Animator anim;

        private Camera mainCamera;
        private Player playerScript;

        private GameObject pausePanel;
        private GameObject directionStatus;
        private Animator dirAnim;
        private TextMeshProUGUI dirText;

        // 0: up, 1: down, 2: left, 3: right
        private readonly Transform[] arrows = new Transform[4];
        private readonly SpriteRenderer[] arrowPanels = new SpriteRenderer[4];
        private readonly Vector2[] arrowRoundedPos = new Vector2[4];
        private Vector2 flooredPlayerStartPos;

        private Transform character;
        private BoxCollider2D characterBoxCollider;
        private Vector2 originalSize = new Vector2();

        #region Start
        private void Start()
        {
            ArrowList();
            ArrowPanelList();

            pausePanel = GameObject.Find("PausePanel");
            directionStatus = GameObject.Find("DirectionStatus");
            dirAnim = directionStatus.GetComponent<Animator>();
            dirText = directionStatus.transform.Find("DirectionText").GetComponent<TextMeshProUGUI>();

            mainCamera = CameraInstance.Instance.GetComponent<Camera>();

            character = this.transform.Find("Character");
            anim = character.GetComponent<Animator>();
            characterBoxCollider = character.GetComponent<BoxCollider2D>();

            playerScript = Player.Instance;
            flooredPlayerStartPos = Utils.RoundPosition(playerScript.transform.position);

            ArrowRoundedPositions();
            originalSize = characterBoxCollider.size;
            SetAllArrowActive(false);
        }

        private void ArrowList()
        {
            for (int i = 0; i < 4; i++)
            {
                arrows[i] = transform.Find("DirectionPanelSelector/" + Utils.dir[i] + "Arrow");
            }
        }

        private void ArrowPanelList()
        {
            for (int i = 0; i < 4; i++)
            {
                arrowPanels[i] = arrows[i].Find(Utils.dir[i] + "ArrowPanel").GetComponent<SpriteRenderer>();
                arrowPanels[i].enabled = false;
            }
        }

        private void ArrowRoundedPositions()
        {
            for (int i = 0; i < 4; i++)
            {
                arrowRoundedPos[i] = Utils.RoundPosition(arrows[i].transform.position);
            }
        }

        #endregion

        #region TouchActions

        private void Update()
        {
            if (!Player.go)
            {
                if (Input.touchCount == 1 && !pausePanel.activeInHierarchy)
                {
                    Touch touch = Input.GetTouch(0);

                    Vector2 roundedPos = Utils.RoundPosition(mainCamera.ScreenToWorldPoint(touch.position));

                    if (touch.phase == TouchPhase.Began)
                    {
                        if (roundedPos == flooredPlayerStartPos)
                        {
                            if (!arrows[0].gameObject.activeInHierarchy)
                            {
                                Debug.Log("Player is touched, and panel is not active");
                                SetAllArrowActive(true);
                                characterBoxCollider.size = new Vector2(4f, 4f);
                            }
                            else
                            {
                                Debug.Log("");
                                SetAllArrowActive(false);
                                characterBoxCollider.size = originalSize;
                            }
                        }
                    }
                    else if (touch.phase == TouchPhase.Moved)
                    {
                        if (Time.frameCount % 3 == 0)
                        {
                            for (int i = 0; i < 4; i++)
                            {
                                if (roundedPos == arrowRoundedPos[i])
                                {
                                    SetArrowPanelActive(i, true);
                                }
                                else
                                {
                                    SetArrowPanelActive(i, false);
                                }
                            }
                        }
                    }
                    else if (touch.phase == TouchPhase.Ended && arrows[0].gameObject.activeInHierarchy)
                    {
                        if (roundedPos == arrowRoundedPos[0])
                        {
                            playerScript.setMoveUpTrue();           // it determines which way the character will go when you start the game
                            DirectionSet("GoIdleUp", "GoIdle", "Up");
                            //StartCoroutine(SetArrowPanelActive(0));
                        }
                        else if (roundedPos == arrowRoundedPos[1])
                        {
                            playerScript.setMoveDownTrue();
                            DirectionSet("GoIdle", "GoIdleUp", "Down");
                            // StartCoroutine(SetArrowPanelActive(1));
                        }
                        else if (roundedPos == arrowRoundedPos[2])
                        {
                            playerScript.setMoveLeftTrue();
                            if (character.transform.rotation.y != 0)
                            {                        // character rotates to selected direction
                                character.transform.eulerAngles = new Vector3(0, 0, 0);
                            }
                            DirectionSet("GoIdle", "GoIdleUp", "Left");
                            // StartCoroutine(SetArrowPanelActive(2));
                        }
                        else if (roundedPos == arrowRoundedPos[3])
                        {
                            playerScript.setMoveRightTrue();
                            if (character.transform.rotation.y != 180)
                            {
                                character.transform.eulerAngles = new Vector3(0, 180, 0);
                            }
                            DirectionSet("GoIdle", "GoIdleUp", "Right");
                            // StartCoroutine(SetArrowPanelActive(3));
                        }
                        else if (roundedPos == flooredPlayerStartPos)
                        {
                            return;
                        }
                        else
                        {
                            SetAllArrowActive(false);
                            SetArrowPanelActive(false);
                            Debug.Log("Nothing selected");
                            return;
                        }
                        SetAllArrowActive(false);
                        SetArrowPanelActive(false);
                        dirAnim.SetTrigger("Show");
                        characterBoxCollider.size = originalSize;
                    }
                }
            }

            else
            {
                Destroy(GameObject.Find("DirectionPanelSelector"));
                Destroy(this);
            }
        }
        #endregion

        #region DirectionPanelSelector

        private void DirectionSet(string activate, string deactivate, string direction)
        {
            anim.SetBool(activate, true);
            anim.SetBool(deactivate, false);
            dirText.text = "Start direction: " + direction;
        }

        private void SetAllArrowActive(bool activate)
        {
            for (int i = 0; i < arrows.Length; i++)
            {
                arrows[i].gameObject.SetActive(activate);
            }
        }

        private void SetArrowPanelActive(int index, bool isActive)
        {
            arrowPanels[index].enabled = isActive;
        }

        private void SetArrowPanelActive(bool isActive)
        {
            for (int i = 0; i < arrowPanels.Length; i++)
            {
                arrowPanels[i].enabled = isActive;
            }
        }

        private IEnumerator SetArrowPanelActive(int index)
        {
            arrowPanels[index].enabled = true;
            yield return new WaitForSeconds(5f);
            arrowPanels[index].enabled = false;
        }
        #endregion

    }
}
