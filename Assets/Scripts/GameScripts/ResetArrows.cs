﻿using Character;
using UnityEngine;
using UnityEngine.UI;

namespace Arrow
{
    public class ResetArrows : MonoBehaviour
    {
        private readonly float arrowSize = .72f;
        private RawImage cleanImg;
        private Vector2[] initialArrowLocations = new Vector2[4];
        private Player playerScript;

        void Start()
        {
            playerScript = Player.Instance;

            initialArrowLocations = playerScript.initialArrowLocations;
            cleanImg = this.gameObject.GetComponent<RawImage>();
            cleanImg.enabled = false;
        }

        // Update is called once per frame
        void Update()
        {
            if (Time.frameCount % 6 == 0)
            {
                if (!Player.go)
                {
                    if (ArrowCounterScript.Instance.arrowCount > 0)
                    {
                        if (!cleanImg.enabled)
                            cleanImg.enabled = true;
                    }
                    else
                    {
                        if (cleanImg.enabled)
                            cleanImg.enabled = false;
                    }
                }
                else
                {
                    Destroy(this.cleanImg);
                    Destroy(this);
                }
            }
        }

        public void resetAllArrowPositionAndSize()
        {
            GameObject[] uArrows = GameObject.FindGameObjectsWithTag("UpArrow");
            GameObject[] dArrows = GameObject.FindGameObjectsWithTag("DownArrow");
            GameObject[] lArrows = GameObject.FindGameObjectsWithTag("LeftArrow");
            GameObject[] rArrows = GameObject.FindGameObjectsWithTag("RightArrow");

            ArrowCounterScript.Instance.arrowCount = 0;

            resetArrowsOfType(uArrows, 0);
            resetArrowsOfType(dArrows, 1);
            resetArrowsOfType(lArrows, 2);
            resetArrowsOfType(rArrows, 3);
        }

        private void resetArrowsOfType(GameObject[] arrowType, int location)
        {
            for (int i = 0; i < arrowType.Length; i++)
            {
                if ((Vector2)arrowType[i].transform.position != this.initialArrowLocations[location])
                {
                    Vector2 wrongArrowRuntimeLocation = arrowType[i].GetComponent<ArrowScript>().wrongArrowRuntimeLocation;
                    if (playerScript.wrongLocations.Contains(wrongArrowRuntimeLocation))
                    {
                        playerScript.wrongLocations.Remove(wrongArrowRuntimeLocation);
                    }

                    arrowType[i].transform.position = this.initialArrowLocations[location];
                    arrowType[i].transform.localScale = new Vector2(arrowSize, arrowSize);
                    arrowType[i].SetActive(true);
                }
            }
        }
    }
}
