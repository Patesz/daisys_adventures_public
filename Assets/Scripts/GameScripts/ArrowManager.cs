﻿using UnityEngine;

namespace Arrow
{
    public class ArrowManager : MonoBehaviour
    {
        private GameObject arrowParent;
        public static ArrowManager Instance;

        private void Awake()
        {
            Instance = this;
        }

        private void Start()
        {
            arrowParent = GameObject.Find("Arrows");
        }

        public void ActivateArrowScript(bool activate)
        {
            ArrowScript[] arrows = arrowParent.GetComponentsInChildren<ArrowScript>();

            for (int i = 0; i < arrows.Length; i++)
            {
                arrows[i].isDragable = activate;
            }
        }

        public void DestroyArrowScripts()
        {
            ArrowScript[] arsc = arrowParent.GetComponentsInChildren<ArrowScript>();
            for (int i = 0; i < arsc.Length; i++)
            {
                Destroy(arsc[i]);
            }
        }

    }
}
