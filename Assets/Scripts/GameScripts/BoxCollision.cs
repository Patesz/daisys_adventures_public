using Character;
using UnityEngine;

namespace GameComponent
{
    public class BoxCollision : MonoBehaviour
    {
        private Vector2 targetPosition = new Vector2();
        private Vector2 boxPrevWrongLoc = new Vector2();

        private SpriteRenderer spriteRenderer;

        private char direction;
        private bool moveBox = false;
        public float speedWithBox { get; private set; }
        private float originalPlayerSpeed;

        private Player playerScript;
        private GameObject[] teleport;

        private void Start()
        {
            targetPosition = this.transform.position;
            spriteRenderer = this.gameObject.GetComponent<SpriteRenderer>();

            playerScript = GameObject.Find("GameContainer/Player").GetComponent<Player>();
            playerScript.wrongLocations.Add(Utils.RoundPosition(transform.position));
            originalPlayerSpeed = playerScript.speed;
            speedWithBox = originalPlayerSpeed * 0.5f;

            teleport = GameObject.FindGameObjectsWithTag("Teleport");
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.name == "Character")
            {
                Debug.Log("Box collided with player");

                boxPrevWrongLoc = Utils.RoundPosition(transform.position);
                if (playerScript.wrongLocations.Contains(boxPrevWrongLoc))
                {
                    playerScript.wrongLocations.Remove(boxPrevWrongLoc);
                }

                moveBox = true;
                playerScript.speed = speedWithBox;
                playerScript.anim.speed /= 2;
                direction = getDirection();
            }
        }

        private char getDirection()
        {
            if (playerScript.moveUp)
            {
                return 'u';
            }
            else if (playerScript.moveDown)
            {
                return 'd';
            }
            else if (playerScript.moveLeft)
            {
                return 'l';
            }
            else
            {
                return 'r';
            }
        }

        private void Update()
        {
            if (moveBox && Player.go)
            {
                Move(targetPosition);

                if ((Vector2)this.transform.position == targetPosition)
                {
                    if (direction == 'l')
                    {
                        if (teleport != null)
                            checkTeleport(-0.5f, 0f);
                        targetPosition = new Vector2(this.transform.position.x - 1, this.transform.position.y);
                    }
                    else if (direction == 'r')
                    {
                        if (teleport != null)
                            checkTeleport(+0.5f, 0f);
                        targetPosition = new Vector2(this.transform.position.x + 1, this.transform.position.y);
                    }
                    else if (direction == 'u')
                    {
                        if (teleport != null)
                            checkTeleport(0f, -0.5f);
                        spriteRenderer.sortingOrder = 2;
                        targetPosition = new Vector2(this.transform.position.x, this.transform.position.y + 1);
                    }
                    else
                    {
                        if (teleport != null)
                            checkTeleport(0f, -0.5f);
                        spriteRenderer.sortingOrder = 10;
                        targetPosition = new Vector2(this.transform.position.x, this.transform.position.y - 1);
                    }

                    if (direction != getDirection())
                    {
                        targetPosition = this.transform.position;
                        Vector2 roundedBoxPos = Utils.RoundPosition(targetPosition);
                        moveBox = false;
                        playerScript.speed = originalPlayerSpeed;
                        playerScript.anim.speed *= 2;

                        playerScript.wrongLocations.Add(Utils.RoundPosition(transform.position));

                    }
                }
            }
        }

        private void checkTeleport(float x, float y)
        {
            for (int i = 0; i < teleport.Length; i++)
            {
                if (Utils.RoundPosition(targetPosition) == Utils.RoundPosition(teleport[i].transform.position))
                {
                    Vector2 otherTeleport;
                    string name = teleport[i].name;
                    if (name == "Teleport_a")
                    {
                        otherTeleport = teleport[i].transform.parent.Find("Teleport_b").transform.position;
                    }
                    else
                    {
                        otherTeleport = teleport[i].transform.parent.Find("Teleport_a").transform.position;
                    }

                    this.transform.position = new Vector2(otherTeleport.x + x, otherTeleport.y + y);
                    break;
                }
            }
        }

        private void Move(Vector2 targetPos)
        {
            Vector2 boxMove = Vector2.MoveTowards(this.transform.position, targetPos, speedWithBox * Time.deltaTime);
            this.transform.position = boxMove;
        }
    }
}