﻿using UnityEngine;
using UnityEngine.Tilemaps;

public class TileScript : MonoBehaviour
{
    public Tilemap map;
    public Tilemap darkMap;
    public Tilemap blurredMap;

    public Tile darkTile;
    public Tile blurredTile;

    // Start is called before the first frame update
    void Start()
    {
        map.origin = blurredMap.origin = darkMap.origin;
        map.size = blurredMap.size = darkMap.size;
        
        foreach (Vector3Int pos in darkMap.cellBounds.allPositionsWithin)
        {
            darkMap.SetTile(pos,darkTile);
        }

        foreach (Vector3Int pos in blurredMap.cellBounds.allPositionsWithin)
        {
            darkMap.SetTile(pos,blurredTile);
        }
    }
}
