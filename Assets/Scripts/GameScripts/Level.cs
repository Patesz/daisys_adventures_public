﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Level", menuName = "Level")]
public class Level : ScriptableObject {
    
    [Header("Arrows")]
    public int[] dirArrowCount = { 1, 1, 1, 1 };

    [Header("Magnify Glass camera bg color")]
    public Color32 bgColor = new Color32( 78, 159, 6, 255 );
}

