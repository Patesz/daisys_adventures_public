﻿using Character;
using UnityEngine;

namespace GameComponent
{
    public class SpawnDirtParticles : MonoBehaviour
    {
        private Transform mudParticle;

        private void Start()
        {

            mudParticle = this.transform.Find("MudParticleEffect");
            if (mudParticle == null)
            {
                Debug.LogError("Particle system not found!");
                Destroy(this.gameObject);
            }
        }

        private void Update()
        {
            if (Player.go)
            {
                delayedSpawn(1f);
            }
        }

        private float timer = 0f;

        private void delayedSpawn(float waitTime)
        {
            timer += Time.deltaTime;
            if (timer > waitTime)
            {
                mudParticle.transform.position = new Vector2(transform.position.x, transform.position.y - 0.475f);
                mudParticle.GetComponent<ParticleSystem>().Play();
                timer = 0f;
            }
        }
    }
}
