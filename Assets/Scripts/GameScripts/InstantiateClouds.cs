using System.Collections;
using UnityEngine;

namespace GameComponent
{
    public class InstantiateClouds : MonoBehaviour
    {
        private const int maxInstance = 7;

        private readonly GameObject[] instanceObject = new GameObject[maxInstance];
        private readonly float[] randSpeed = new float[maxInstance];

        private int currentInstanceCount = 0;

        private const int coordX = -11;
        private const float yCoordMin = -4.5f, yCoordMax = 5f;
        private const float minSpeed = .5f, maxSpeed = 0.75f;
        private const float scaleMin = 0.8f, scaleMax = 1.2f;
        private const float minTransparency = 0.6f, maxTransparency = 0.8f;

        private float timer = 0.0f;
        private float waitingTime = 7.0f;

        private GameObject CreateCloud()
        {
            GameObject originCloud = new GameObject
            {
                name = "cloud"
            };
            Sprite cloudSprite = Resources.Load<Sprite>("Sprites/Cloud_1");
            Debug.Log(cloudSprite);
            if (cloudSprite != null)
            {
                SpriteRenderer cloudRenderer = originCloud.AddComponent<SpriteRenderer>();
                cloudRenderer.sprite = cloudSprite;
                cloudRenderer.sortingOrder = 1000;
                originCloud.transform.position = new Vector2(-100, -100);
                originCloud.transform.parent = this.gameObject.transform;
                return originCloud;
            }
            else
            {
                Debug.LogError("Cloud error: sprite image load failed! GameObject destroyed.");
                Destroy(this.gameObject);
                return null;
            }
        }

        private IEnumerator Start()
        {
            GameObject cloud = null;

            if (PlayerPrefs.GetString("isCloudsEnabled", "enable") == "enable")
            {
                cloud = CreateCloud();
            }
            else
            {
                Destroy(this.gameObject);
            }

            float coordY, scaleXY;

            for (int i = 0; i < maxInstance; i++)
            {
                coordY = Random.Range(yCoordMin, yCoordMax);
                scaleXY = Random.Range(scaleMin, scaleMax);

                randSpeed[i] = Random.Range(minSpeed, maxSpeed);
                
                Vector2 randPos = new Vector2(coordX, coordY);

                int wait_time = Random.Range(2, 10);
                yield return new WaitForSeconds(wait_time);

                // params: GameObject, position, rotation, parent
                instanceObject[i] = Instantiate<GameObject>(cloud, randPos, cloud.transform.rotation, cloud.transform.parent);
                currentInstanceCount++;

                SpriteRenderer spriteRenderer = instanceObject[i].GetComponent<SpriteRenderer>();
                spriteRenderer.color = new Color(1f, 1f, 1f, Random.Range(minTransparency, maxTransparency));

                instanceObject[i].transform.localScale = new Vector2(scaleXY, scaleXY);
            }
        }

        private void Update()
        {
            for (int i = 0; i < currentInstanceCount; i++)
            {
                if (instanceObject[i].transform.position.x < 14)
                {
                    instanceObject[i].transform.Translate(new Vector2(randSpeed[i] * Time.deltaTime, 0));
                }
                else
                {
                    waitingTime = Random.Range(3f, 32f);
                    DelayTransform(i);
                }
            }
        }

        private void DelayTransform(int index)
        {
            timer += Time.deltaTime;
            if (timer > waitingTime)
            {
                timer = 0f;
                TransformToNewRandomPosWithSpeed(index);
            }
        }

        private void TransformToNewRandomPosWithSpeed(int index)
        {
            float coordY = Random.Range(yCoordMin, yCoordMax);

            instanceObject[index].transform.position = new Vector2(coordX, coordY);
            randSpeed[index] = Random.Range(minSpeed, maxSpeed);
        }
    }
}