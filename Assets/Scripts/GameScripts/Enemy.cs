﻿using Character;
using UnityEngine;

namespace GameComponent
{
    public class Enemy : MonoBehaviour
    {
        //      ---        CONFIG       ---         //
        private readonly float changeCoordZ = 2f;

        public float patrolSpeed = 1f;
        public Vector2[] patrolPoints = new Vector2[2];

        private bool patrolBackwards = false;
        private int patrolPointLength;
        private int currPoint = 0;

        private void Start()
        {
            patrolPointLength = patrolPoints.Length;
            patrolPoints[currPoint] = transform.position;
            RotateSelector(currPoint+1, true);
           // Player.Instance.wrongLocations.Add(Utils.RoundPosition(transform.position));
        }

        private void Update()
        {
            if (Player.go)
            {
                if ((Vector2)transform.position == patrolPoints[currPoint])
                {
                    if (!patrolBackwards)
                        currPoint++;
                    else
                    {
                        currPoint--;
                        if (currPoint <= 0)
                        {
                            patrolBackwards = false;
                        }
                    }
                }

                //if current Point is greater or eqauls with the number of patrol points then reset value and start again
                if (currPoint >= patrolPointLength)
                {
                    if (patrolPointLength % 2 == 0)
                    {
                        currPoint = 0;
                    }
                    else
                    {
                        currPoint -= 2;
                        patrolBackwards = true;
                    }
                }
                RotateSelector(currPoint, false);
                Move();
            }
        }
        private void RotateSelector(int currPoint, bool rotateInstantly)
        {
            Vector2 currPos = (Vector2)transform.position;

            // TURN UP
            if (patrolPoints[currPoint].x == currPos.x && patrolPoints[currPoint].y > currPos.y) {
                Rotate(180, rotateInstantly);
            }

            // TURN DOWN
            else if (patrolPoints[currPoint].x == currPos.x && patrolPoints[currPoint].y < currPos.y) {
                Rotate(0, rotateInstantly);
            }

            // TURN LEFT
            else if (patrolPoints[currPoint].x < currPos.x && patrolPoints[currPoint].y == currPos.y) {
                Rotate(270, rotateInstantly);
            }

            // TURN RIGHT
            else {
                Rotate(90, rotateInstantly);
            }
        }

        private void Rotate(int targetRotation, bool rotateInstantly)
        {
            float coordZ = transform.eulerAngles.z;
            if (Mathf.Floor(coordZ)+1 != targetRotation) {
                if (!rotateInstantly)
                    transform.eulerAngles = new Vector3(0f, 0f, coordZ+changeCoordZ);
                else
                    transform.eulerAngles = new Vector3(0f, 0f, targetRotation);
            }
        }

        private void Move()
        {
            // moves the object: 1.current pos, 2.target pos, 3.speed
            transform.position = Vector2.MoveTowards(transform.position, patrolPoints[currPoint], patrolSpeed * Time.deltaTime);
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.name == "Character")
            {
                Player.Instance.showDeathFeedback("It's eating MEEE!!!");
            }
        }
    }
}
