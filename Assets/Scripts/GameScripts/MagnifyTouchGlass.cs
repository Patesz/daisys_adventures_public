﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameComponent
{
    public class MagnifyTouchGlass : MonoBehaviour
    {
        private bool isRight = true;

        private readonly int scrWidth = Screen.width;
        private readonly int scrHeight = Screen.height;

        private Camera magnifyCamera;
        private Camera mainCamera;
        private GameObject magnifyBorder;
        private LineRenderer LeftBorder, RightBorder, TopBorder, BottomBorder; // Reference for lines of magnify glass borders
        private float MGOX, MG0Y; // Magnify Glass Origin X and Y position
        private readonly float MGWidth = Screen.width / 4f, MGHeight = Screen.width / 4f; // Magnify glass width and height
        private Vector2 touchPos;

        public bool stopMagnifyGlass { get; set; }
        public bool switchSide { get; set; }

        private int cellPixelSize;
        private readonly int cellWidthSize = 20; // The game is full width, 20 cell is fill the whole game width
        private readonly int cellHeightSize = 10;
        private int screenHeightDiff; // The game is not full height, this is the diff that complite the gamescreen height

        private GameObject pausePanel;
        private readonly Level currentLevel;

        void Start()
        {
            mainCamera = CameraInstance.Instance.GetComponent<Camera>();
            pausePanel = GameObject.Find("PausePanel");
            pausePanel.SetActive(false);

            CreateMagnifyGlass();
            setBgColor();

            cellPixelSize = scrWidth / cellWidthSize;
            screenHeightDiff = scrHeight - (cellHeightSize * cellPixelSize);
        }

        private void setBgColor()
        {
            magnifyCamera.backgroundColor =
                GameManagerScript.Instance.getLevelBgColor(SceneManager.GetActiveScene().name);
        }

        void Update()
        {
            if (Input.touchCount == 1 && !pausePanel.activeInHierarchy)
            {
                Touch touch = Input.GetTouch(0);
                touchPos = getWorldPosition(touch.position);

                if (touch.phase == TouchPhase.Began)
                {
                    if(isRight)
                    {
                        magnifyCamera.pixelRect = new Rect(scrWidth - MGWidth, (scrHeight - screenHeightDiff / 2) - MGHeight, MGWidth, MGHeight);
                    }
                    else
                    {
                        magnifyCamera.pixelRect = new Rect(0, (scrHeight - screenHeightDiff / 2) - MGHeight, MGWidth, MGHeight);
                    }
                }

                if (touch.phase == TouchPhase.Moved || touch.phase == TouchPhase.Began)
                {
                    if (touchPos.x > 3.7)
                    {
                        magnifyCamera.pixelRect = new Rect(0, (scrHeight - screenHeightDiff / 2) - MGHeight, MGWidth, MGHeight);
                        isRight = false;
                    }
                    else if (touchPos.x < -3.7)
                    {
                        magnifyCamera.pixelRect = new Rect(scrWidth - MGWidth, (scrHeight - screenHeightDiff / 2) - MGHeight, MGWidth, MGHeight);
                        isRight = true;
                    }

                    if (touchPos.x < -9.1)
                    {
                        if (touchPos.y < 4.1 && touchPos.y > -4.1)
                            magnifyCamera.transform.position = new Vector3(-9.1f, touchPos.y, -10f);
                    }
                    else if (touchPos.x > 9.1)
                    {
                        if (touchPos.y < 4.1 && touchPos.y > -4.1)
                            magnifyCamera.transform.position = new Vector3(9.1f, touchPos.y, -10f);
                    }
                    else if (touchPos.y < -4.1)
                    {
                        magnifyCamera.transform.position = new Vector3(touchPos.x, -4.1f, -10f);
                    }
                    else if (touchPos.y > 4.1)
                    {
                        magnifyCamera.transform.position = new Vector3(touchPos.x, 4.1f, -10f);
                    }
                    else
                    {
                        magnifyCamera.transform.position = new Vector3(touchPos.x, touchPos.y, -10f);
                    }
                }
                else if (touch.phase == TouchPhase.Ended)
                {
                    magnifyCamera.pixelRect = new Rect(0, 0, 0, 0);
                }
            }
            else
            {
                magnifyCamera.pixelRect = new Rect(0, 0, 0, 0);
            }
        }

        // Following method creates MagnifyGlass
        private void CreateMagnifyGlass()
        {
            GameObject camera = new GameObject("MagnifyCamera");
            camera.transform.SetParent(this.gameObject.transform);

            MGOX = scrWidth / 2f - MGWidth / 2f;
            MG0Y = scrHeight / 2f - MGHeight / 2f;
            magnifyCamera = camera.AddComponent<Camera>();
            magnifyCamera.depth = 2f;                                               // !!!
            magnifyCamera.pixelRect = new Rect(MGOX, MG0Y, MGWidth, MGHeight);

            magnifyCamera.allowMSAA = false;
            magnifyCamera.allowHDR = false;
            magnifyCamera.useOcclusionCulling = false;

            if (mainCamera.orthographic)
            {
                magnifyCamera.orthographic = true;
                magnifyCamera.orthographicSize = mainCamera.orthographicSize / 5.0f;//+ 1.0f; //eredeti 5 volt
                                                                                    //createBordersForMagniyGlass();
            }
            else
            {
                magnifyCamera.orthographic = false;
                magnifyCamera.fieldOfView = mainCamera.fieldOfView / 10.0f;//3.0f;
            }

            magnifyCamera.pixelRect = new Rect(scrWidth - MGWidth, (scrHeight - screenHeightDiff / 2) - MGHeight, MGWidth, MGHeight);
            magnifyCamera.pixelRect = new Rect(0, 0, 0, 0);
        }

        // Following method sets border of MagnifyGlass
        private void CreateBordersForMagniyGlass()
        {
            magnifyBorder = new GameObject();
            LeftBorder = getLine();
            //LeftBorder.SetVertexCount(2);
            LeftBorder.positionCount = 2;
            LeftBorder.SetPosition(0, new Vector3(getWorldPosition(new Vector2(MGOX, MG0Y)).x, getWorldPosition(new Vector2(MGOX, MG0Y)).y - 0.1f, -1));
            LeftBorder.SetPosition(1, new Vector3(getWorldPosition(new Vector2(MGOX, MG0Y + MGHeight)).x, getWorldPosition(new Vector2(MGOX, MG0Y + MGHeight)).y + 0.1f, -1));
            LeftBorder.transform.parent = magnifyBorder.transform;
            TopBorder = getLine();
            //TopBorder.SetVertexCount(2);
            TopBorder.positionCount = 2;
            TopBorder.SetPosition(0, new Vector3(getWorldPosition(new Vector2(MGOX, MG0Y + MGHeight)).x, getWorldPosition(new Vector2(MGOX, MG0Y + MGHeight)).y, -1));
            TopBorder.SetPosition(1, new Vector3(getWorldPosition(new Vector2(MGOX + MGWidth, MG0Y + MGHeight)).x, getWorldPosition(new Vector2(MGOX + MGWidth, MG0Y + MGHeight)).y, -1));
            TopBorder.transform.parent = magnifyBorder.transform;
            RightBorder = getLine();
            //RightBorder.SetVertexCount(2);
            RightBorder.positionCount = 2;
            RightBorder.SetPosition(0, new Vector3(getWorldPosition(new Vector2(MGOX + MGWidth, MG0Y + MGWidth)).x, getWorldPosition(new Vector2(MGOX + MGWidth, MG0Y + MGWidth)).y + 0.1f, -1));
            RightBorder.SetPosition(1, new Vector3(getWorldPosition(new Vector2(MGOX + MGWidth, MG0Y)).x, getWorldPosition(new Vector2(MGOX + MGWidth, MG0Y)).y - 0.1f, -1));
            RightBorder.transform.parent = magnifyBorder.transform;
            BottomBorder = getLine();
            //BottomBorder.SetVertexCount(2);
            BottomBorder.positionCount = 2;
            BottomBorder.SetPosition(0, new Vector3(getWorldPosition(new Vector2(MGOX + MGWidth, MG0Y)).x, getWorldPosition(new Vector2(MGOX + MGWidth, MG0Y)).y, -1));
            BottomBorder.SetPosition(1, new Vector3(getWorldPosition(new Vector2(MGOX, MG0Y)).x, getWorldPosition(new Vector2(MGOX, MG0Y)).y, -1));
            BottomBorder.transform.parent = magnifyBorder.transform;
        }

        // Following method creates new line for MagnifyGlass's border
        private LineRenderer getLine()
        {
            LineRenderer line = new GameObject("Line").AddComponent<LineRenderer>();
            line.material = new Material(Shader.Find("Diffuse"));
            //line.SetVertexCount(2);
            line.positionCount = 2;
            //line.SetWidth(0.2f, 0.2f);
            line.startWidth = 0.2f;
            line.endWidth = 0.2f;
            //line.SetColors(Color.black, Color.black);
            line.startColor = Color.black;
            line.endColor = Color.black;
            line.useWorldSpace = false;
            return line;
        }
        private void setLine(LineRenderer line)
        {
            line.material = new Material(Shader.Find("Diffuse"));
            //line.SetVertexCount(2);
            line.positionCount = 2;
            //line.SetWidth(0.2f, 0.2f);
            line.startWidth = 0.2f;
            line.endWidth = 0.2f;
            //line.SetColors(Color.black, Color.black);
            line.startColor = Color.black;
            line.endColor = Color.black;
            line.useWorldSpace = false;
        }

        // Following method calculates world's point from screen point as per camera's projection type
        public Vector3 getWorldPosition(Vector2 screenPos)
        {
            Vector3 worldPos;
            //if (mainCamera.orthographic)
            //{
            worldPos = mainCamera.ScreenToWorldPoint(screenPos);
            worldPos.z = mainCamera.transform.position.z;
            //}
            /*else
            {
                worldPos = mainCamera.ScreenToWorldPoint(new Vector3(screenPos.x, screenPos.y, mainCamera.transform.position.z));
                worldPos.x *= -1;
                worldPos.y *= -1;
            }*/
            return worldPos;
        }
    }
}