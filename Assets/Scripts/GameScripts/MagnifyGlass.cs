﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameComponent
{
    public class MagnifyGlass : MonoBehaviour
    {
        private Camera magnifyCamera;
        private GameObject magnifyBorders;
        private LineRenderer LeftBorder, RightBorder, TopBorder, BottomBorder; // Reference for lines of magnify glass borders
        private float MGOX, MG0Y; // Magnify Glass Origin X and Y position
        private readonly float  MGWidth = Screen.width / 4f, MGHeight = Screen.width / 4f; // Magnify glass width and height
        private Vector3 mousePos;

        public bool stopMagnifyGlass { get; set; }
        public bool switchSide { get; set; }

        private int cellPixelSize;
        private readonly int cellWidthSize = 20; // The game is full width, 20 cell is fill the whole game width
        private readonly int cellHeightSize = 10;
        private int screenHeightDiff; // The game is not full height, this is the diff that complite the gamescreen height

        private GameObject pausePanel;
        private GameObject winPanel;
        private Level currentLevel;

        void Start()
        {
            stopMagnifyGlass = true;

            pausePanel = GameObject.Find("PausePanel");
            pausePanel.SetActive(false);
            winPanel = GameObject.Find("WinPanel");

            CreateMagnifyGlass();
            magnifyCamera.pixelRect = new Rect(Screen.width - MGWidth, (Screen.height - screenHeightDiff / 2) - MGHeight, MGWidth, MGHeight);

            cellPixelSize = Screen.width / cellWidthSize;
            screenHeightDiff = Screen.height - (cellHeightSize * cellPixelSize);
            switchSide = false;
        }

        void Update()
        {
            // Following lines set the camera's pixelRect and camera position at mouse position
            /*magnifyCamera.pixelRect = new Rect(Input.mousePosition.x - MGWidth / 2.0f, Input.mousePosition.y - MGHeight / 2.0f, MGWidth, MGHeight); */
            mousePos = getWorldPosition(Input.mousePosition);
            magnifyCamera.transform.position = mousePos;
            /*mousePos.z = 0;
            magnifyBorders.transform.position = mousePos;
            Debug.Log(mousePos);*/

            /*
            // a borderrel legyen mozgatható, de mégis mintha középen lenne a camera
            mousePos = getWorldPosition(Input.mousePosition);

            //Checks whether the mouse is over the sprite
            bool overSprite = LeftBorder.GetComponent<LineRenderer>().bounds.Contains(mousePos)
                                || RightBorder.GetComponent<LineRenderer>().bounds.Contains(mousePos)
                                || TopBorder.GetComponent<LineRenderer>().bounds.Contains(mousePos)
                                || BottomBorder.GetComponent<LineRenderer>().bounds.Contains(mousePos);

            //If it's over the sprite
            if (overSprite)
            {
                //If we've pressed down on the mouse (or touched on the iphone)
                if (Input.GetButton("Fire1"))
                {
                    magnifyCamera.pixelRect = new Rect(Input.mousePosition.x - MGWidth / 2.0f, Input.mousePosition.y - MGHeight / 2.0f, MGWidth, MGHeight);
                    magnifyCamera.transform.position = mousePos;
                    mousePos.z = 0;
                    magnifyBorders.transform.position = mousePos;
                }
            }*/

            // igen itt simán kihagyhattam volna a logikai változót és már rögtön itt beállíthattam volna, hogy lűtszódjon-e vagy nem
            // DE ez a minimum módosítás elve, minimális módosítással működő koncepció

            if (Input.GetMouseButton(0) && !pausePanel.activeInHierarchy || (Vector2)winPanel.transform.position == new Vector2(0f, 0f))
            {
                stopMagnifyGlass = false;

                if (mousePos.x >= 4)
                {
                    switchSide = true;
                }
                if (mousePos.x <= -4)
                {
                    switchSide = false;
                }
            }
            else
            {
                stopMagnifyGlass = true;
            }

            if (stopMagnifyGlass)
            {
                magnifyCamera.pixelRect = new Rect(0, 0, 0, 0);
            }
            else
            {
                magnifyCamera.pixelRect = new Rect(Screen.width - MGWidth, (Screen.height - screenHeightDiff / 2) - MGHeight, MGWidth, MGHeight);
            }
            if (switchSide && !stopMagnifyGlass)
            {
                magnifyCamera.pixelRect = new Rect(0, (Screen.height - screenHeightDiff / 2) - MGHeight, MGWidth, MGHeight);
            }

        }

        // Following method creates MagnifyGlass
        private void CreateMagnifyGlass()
        {
            GameObject camera = new GameObject("MagnifyCamera");
            camera.transform.SetParent(this.gameObject.transform);

            MGOX = Screen.width / 2f - MGWidth / 2f;
            MG0Y = Screen.height / 2f - MGHeight / 2f;
            magnifyCamera = camera.AddComponent<Camera>();
            magnifyCamera.depth = 2f;                                               // !!!
            magnifyCamera.pixelRect = new Rect(MGOX, MG0Y, MGWidth, MGHeight);
            magnifyCamera.transform.position = new Vector2(0, 0);

            magnifyCamera.allowMSAA = false;
            magnifyCamera.allowHDR = false;
            magnifyCamera.useOcclusionCulling = false;

            string levelName = SceneManager.GetActiveScene().name;
            currentLevel = GameManagerScript.Instance.getLevel(levelName);
            magnifyCamera.backgroundColor = currentLevel.bgColor;

            if (Camera.main.orthographic)
            {
                magnifyCamera.orthographic = true;
                magnifyCamera.orthographicSize = Camera.main.orthographicSize / 5.0f;//+ 1.0f; //eredeti 5 volt
                                                                                     //createBordersForMagniyGlass();
            }
            else
            {
                magnifyCamera.orthographic = false;
                magnifyCamera.fieldOfView = Camera.main.fieldOfView / 10.0f;//3.0f;
            }

        }

        // Following method sets border of MagnifyGlass
        private void CreateBordersForMagniyGlass()
        {
            magnifyBorders = new GameObject();
            LeftBorder = getLine();
            //LeftBorder.SetVertexCount(2);
            LeftBorder.positionCount = 2;
            LeftBorder.SetPosition(0, new Vector3(getWorldPosition(new Vector3(MGOX, MG0Y, 0)).x, getWorldPosition(new Vector3(MGOX, MG0Y, 0)).y - 0.1f, -1));
            LeftBorder.SetPosition(1, new Vector3(getWorldPosition(new Vector3(MGOX, MG0Y + MGHeight, 0)).x, getWorldPosition(new Vector3(MGOX, MG0Y + MGHeight, 0)).y + 0.1f, -1));
            LeftBorder.transform.parent = magnifyBorders.transform;
            TopBorder = getLine();
            //TopBorder.SetVertexCount(2);
            TopBorder.positionCount = 2;
            TopBorder.SetPosition(0, new Vector3(getWorldPosition(new Vector3(MGOX, MG0Y + MGHeight, 0)).x, getWorldPosition(new Vector3(MGOX, MG0Y + MGHeight, 0)).y, -1));
            TopBorder.SetPosition(1, new Vector3(getWorldPosition(new Vector3(MGOX + MGWidth, MG0Y + MGHeight, 0)).x, getWorldPosition(new Vector3(MGOX + MGWidth, MG0Y + MGHeight, 0)).y, -1));
            TopBorder.transform.parent = magnifyBorders.transform;
            RightBorder = getLine();
            //RightBorder.SetVertexCount(2);
            RightBorder.positionCount = 2;
            RightBorder.SetPosition(0, new Vector3(getWorldPosition(new Vector3(MGOX + MGWidth, MG0Y + MGWidth, 0)).x, getWorldPosition(new Vector3(MGOX + MGWidth, MG0Y + MGWidth, 0)).y + 0.1f, -1));
            RightBorder.SetPosition(1, new Vector3(getWorldPosition(new Vector3(MGOX + MGWidth, MG0Y, 0)).x, getWorldPosition(new Vector3(MGOX + MGWidth, MG0Y, 0)).y - 0.1f, -1));
            RightBorder.transform.parent = magnifyBorders.transform;
            BottomBorder = getLine();
            //BottomBorder.SetVertexCount(2);
            BottomBorder.positionCount = 2;
            BottomBorder.SetPosition(0, new Vector3(getWorldPosition(new Vector3(MGOX + MGWidth, MG0Y, 0)).x, getWorldPosition(new Vector3(MGOX + MGWidth, MG0Y, 0)).y, -1));
            BottomBorder.SetPosition(1, new Vector3(getWorldPosition(new Vector3(MGOX, MG0Y, 0)).x, getWorldPosition(new Vector3(MGOX, MG0Y, 0)).y, -1));
            BottomBorder.transform.parent = magnifyBorders.transform;
        }

        // Following method creates new line for MagnifyGlass's border
        private LineRenderer getLine()
        {
            LineRenderer line = new GameObject("Line").AddComponent<LineRenderer>();
            line.material = new Material(Shader.Find("Diffuse"));
            //line.SetVertexCount(2);
            line.positionCount = 2;
            //line.SetWidth(0.2f, 0.2f);
            line.startWidth = 0.2f;
            line.endWidth = 0.2f;
            //line.SetColors(Color.black, Color.black);
            line.startColor = Color.black;
            line.endColor = Color.black;
            line.useWorldSpace = false;
            return line;
        }
        private void setLine(LineRenderer line)
        {
            line.material = new Material(Shader.Find("Diffuse"));
            //line.SetVertexCount(2);
            line.positionCount = 2;
            //line.SetWidth(0.2f, 0.2f);
            line.startWidth = 0.2f;
            line.endWidth = 0.2f;
            //line.SetColors(Color.black, Color.black);
            line.startColor = Color.black;
            line.endColor = Color.black;
            line.useWorldSpace = false;
        }

        // Following method calculates world's point from screen point as per camera's projection type
        public Vector3 getWorldPosition(Vector3 screenPos)
        {
            Vector3 worldPos;
            if (Camera.main.orthographic)
            {
                worldPos = Camera.main.ScreenToWorldPoint(screenPos);
                worldPos.z = Camera.main.transform.position.z;
            }
            else
            {
                worldPos = Camera.main.ScreenToWorldPoint(new Vector3(screenPos.x, screenPos.y, Camera.main.transform.position.z));
                worldPos.x *= -1;
                worldPos.y *= -1;
            }
            return worldPos;
        }
    }
}