using Character;
using System.Collections;
using UnityEngine;

namespace GameComponent
{
    public class SpawnPortParticles : MonoBehaviour
    {

        private readonly Transform[] portParticleInstance = new Transform[2];
        private readonly Transform[] ports = new Transform[2];
        private readonly Teleport[] tpScript = new Teleport[2];

        private GameObject player;
        private Player playerScript;

        void Start()
        {
            ports[0] = this.transform.Find("Teleport_a");
            ports[1] = this.transform.Find("Teleport_b");

            tpScript[0] = ports[0].GetComponent<Teleport>();
            tpScript[1] = ports[1].GetComponent<Teleport>();

            Transform portParticle = transform.parent.Find("PortParticle");
            //   what             where                   rotation              place in hierarchy
            portParticleInstance[0] = Instantiate<Transform>(portParticle, ports[0].position, portParticle.transform.rotation, ports[0].transform);
            portParticleInstance[1] = Instantiate<Transform>(portParticle, ports[1].position, portParticle.transform.rotation, ports[1].transform);

            playerScript = Player.Instance;
            player = playerScript.gameObject;

        }

        private void Update()
        {
            if (tpScript[0].teleported)
            {
                tpScript[0].teleported = false;
                teleportTo(1);
            }
            if (tpScript[1].teleported)
            {
                tpScript[1].teleported = false;
                teleportTo(0);
            }
        }

        private void teleportTo(int index)
        {
            portParticleInstance[0].GetComponent<ParticleSystem>().Play();
            portParticleInstance[1].GetComponent<ParticleSystem>().Play();

            player.transform.position = new Vector2(ports[index].position.x, ports[index].position.y + 0.25f);
            playerScript.targetXPos = player.transform.position.x;
            playerScript.targetYPos = player.transform.position.y;

            Collider2D port_coll = ports[index].gameObject.GetComponent<BoxCollider2D>();
            StartCoroutine(EnableTeleport(1 / playerScript.speed, port_coll));
        }

        private IEnumerator EnableTeleport(float time, Collider2D port_coll)
        {
            port_coll.enabled = false;
            yield return new WaitForSeconds(time);
            port_coll.enabled = true;
        }
    }
}
