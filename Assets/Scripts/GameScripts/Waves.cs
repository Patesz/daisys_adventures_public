﻿using Character;
using System.Collections.Generic;
using UnityEngine;

public class Waves : MonoBehaviour
{
    private List<Vector2> waterTilePos = new List<Vector2>();
    private Transform wave;

    void Start()
    {
        waterTilePos = Player.Instance.waterTilePositions;
        wave = transform.Find("Wave");

        instantiateRandomWaves();
    }

    private void instantiateRandomWaves()
    {
        int len = waterTilePos.Count;
        for (int i = 0; i < len; i++)
        {
            int rand = Random.Range(0, 5);
            if (rand == 2)
            {
                float rotationX = Random.Range(0, 360);
                float rotationY = Random.Range(0, 360);
                Instantiate(wave, new Vector2(waterTilePos[i].x + .5f, waterTilePos[i].y + .5f), 
                    wave.rotation, this.transform);
            }
        }
    }
}
