﻿using Character;

using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

namespace Arrow
{
    public class ArrowCounterScript : MonoBehaviour
    {
        public static ArrowCounterScript Instance { get; set; }

        [HideInInspector]
        public int arrowCount = 0;

        private Level currentLevel;
        private readonly TextMeshProUGUI[] dirArrowText = new TextMeshProUGUI[4];
        public int[] dirArrowCount { get; private set; }
        private readonly int[] dirInstanceCount = new int[4];
        private readonly Transform[] dirArrows = new Transform[4];

        private void Awake()
        {
            Instance = this;
        }

        #region Start
        // Resources.Load is slow in GameManagerScript
        private void Start()
        {
            dirArrowCount = new int[4];

            currentLevel = GameManagerScript.Instance.getLevel(SceneManager.GetActiveScene().name);

            GameObject arrowCounterContainer = GameObject.Find("ArrowCount");

            for (int i = 0; i < 4; i++)
            {
                dirArrowText[i] = arrowCounterContainer.transform.Find(Utils.direction[i] + "ArrowCount").GetComponent<TextMeshProUGUI>();
            }

            InitialArrowCounterState();
            DirArrowInstanceCounter();
            FindArrows();
        }
        // ArrowManager ResetArrows uses this function on button click
        public void InitialArrowCounterState()
        {
            for (int i = 0; i < 4; i++)
            {
                dirArrowCount[i] = currentLevel.dirArrowCount[i];
                dirArrowText[i].text = "x" + dirArrowCount[i].ToString();
            }
        }

        private void DirArrowInstanceCounter()
        {
            for (int i = 0; i < 4; i++)
            {
                dirInstanceCount[i] = dirArrowCount[i];
            }
        }

        private void FindArrows()
        {
            GameObject arrows = GameObject.Find("Arrows");

            for (int i = 0; i < 4; i++)
            {
                dirArrows[i] = arrows.transform.Find(Utils.direction[i] + "Arrow");
            }

        }

        #endregion

        #region Runtime
        public void UpdateArrowCounterSelector(string tag, bool isIncrement)
        {
            for (int i = 0; i < 4; i++)
            {
                if (tag == Utils.direction[i] + "Arrow")
                {
                    UpdateArrowCounter(isIncrement, i);
                    return;
                }
            }
        }

        private void UpdateArrowCounter(bool isIncrement, int id)
        {
            if (isIncrement)
            {
                arrowCount--;

                for (int i = 0; i < 4; i++)
                {
                    if (id == i)
                    {
                        this.dirArrowCount[i]++;
                        dirArrowText[i].text = "x" + dirArrowCount[i].ToString();
                        return;
                    }
                }
            }
            else
            {
                arrowCount++;

                for (int i = 0; i < 4; i++)
                {
                    if (id == i)
                    {
                        this.dirArrowCount[i]--;
                        dirArrowText[i].text = "x" + dirArrowCount[i].ToString();
                        if (dirInstanceCount[i] > 1)
                        {
                            Transform newArrow = Instantiate(dirArrows[i], Player.Instance.initialArrowLocations[i], dirArrows[i].rotation, dirArrows[i].parent);
                            if ((Vector2)newArrow.localScale != new Vector2(.72f, .72f))
                            {
                                newArrow.localScale = new Vector2(.72f, .72f);
                            }
                            dirInstanceCount[i]--;
                        }
                    }
                }
            }
        }
        #endregion
    }
}
