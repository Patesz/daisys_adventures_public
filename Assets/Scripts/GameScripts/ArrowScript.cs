﻿using Character;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace Arrow
{
    public class ArrowScript : MonoBehaviour
    {
        private Camera mainCamera;
        private SpriteRenderer arrowSpriteRenderer;
        private SpriteRenderer shadowSpriteRenderer;
        private GameObject wrongPositionObject;
        private GameObject rightPositionObject;
        private Tilemap tileMap;
        private Player player;

        private bool updateArrowCounter = true;
        private float previousArrowPosX;
        private float previousArrowPosY;

        public Vector2 wrongArrowRuntimeLocation { get; set; }
        private ParticleSystem spawnEffectPS;
        private BoxCollider2D boxCollider;
        private string originalTag;

        [HideInInspector]
        public Vector2 originalPosition;
        [HideInInspector]
        public bool isDragable = true;

        #region ArrowDragAndDrop
        private void OnMouseDown()
        {
            if (isDragable)
            {
                this.tag = "CantCollideWithMe";
                shadowSpriteRenderer.enabled = true;
                if (checkArrowLocation())
                {               // mikor lenyomod az egérgombot, akkor megnézi a nyíl pillanatnyi pozícióját
                    updateArrowCounter = true;        // amennyiben ez megegyezik az eredeti pozíciója, akkor a számláló változhat
                }
                else
                {
                    updateArrowCounter = false;       // ha a nyíl már a pályán van, akkor ne változhasson az adott nyíl számlálója
                    if (player.wrongLocations.Contains(wrongArrowRuntimeLocation))
                    {
                        player.wrongLocations.Remove(wrongArrowRuntimeLocation);
                    }
                }
            }
        }

        private void OnMouseDrag()
        {
            if (isDragable)
            {
                Vector2 pos = getMousePosition();
                Vector2Int roundedPos = Utils.RoundV2IntPosition(pos);

                this.transform.position = pos;

                bool isGoodPos = ShowWrongLocation(roundedPos.x, roundedPos.y);

                if (roundedPos.x < 2 && roundedPos.x > -3 && roundedPos.y < -4 && roundedPos.y > -6)
                {
                    tileMap.SetTileFlags(new Vector3Int(roundedPos.x, roundedPos.y, 0), TileFlags.None); // enables tilemap coloring
                    tileMap.SetColor(new Vector3Int(roundedPos.x, roundedPos.y, 0), Color.green);
                }

                else if (!isGoodPos)
                {
                    tileMap.SetTileFlags(new Vector3Int(roundedPos.x, roundedPos.y, 0), TileFlags.None); // enables tilemap coloring
                    tileMap.SetColor(new Vector3Int(roundedPos.x, roundedPos.y, 0), Color.red);
                }
                else
                {
                    tileMap.SetTileFlags(new Vector3Int(roundedPos.x, roundedPos.y, 0), TileFlags.None);
                    tileMap.SetColor(new Vector3Int(roundedPos.x, roundedPos.y, 0), Color.gray);
                }

                if (previousArrowPosX != roundedPos.x || previousArrowPosY != roundedPos.y)
                {
                    tileMap.SetColor(new Vector3Int((int)previousArrowPosX, (int)previousArrowPosY, 0), Color.white);
                    previousArrowPosX = roundedPos.x;
                    previousArrowPosY = roundedPos.y;
                }
            }
        }

        private void OnMouseUp()
        {
            if (isDragable)
            {
                this.tag = originalTag;
                shadowSpriteRenderer.enabled = false;

                wrongPositionObject.transform.position = new Vector2(100, 100);
                rightPositionObject.transform.position = new Vector2(100, 100);

                Color white = Color.white;
                for (int i = -10; i < 10; i++)
                {
                    for (int j = -5; j < 5; j++)
                    {
                        tileMap.SetColor(new Vector3Int(i, j, 0), white);
                    }
                }

                float tpX = transform.position.x;
                float tpY = transform.position.y;

                if (gameObject.GetComponent<SpriteRenderer>().color.a != 1f)
                {
                    gameObject.GetComponent<SpriteRenderer>().color = new Color32(204, 53, 121, 255);
                }

                Vector2Int roundedPos = Utils.RoundV2IntPosition(getMousePosition());
                if (isGoodLocations(roundedPos.x, roundedPos.y) == 3 && tpX < 10 && tpX > -10 && tpY > -5 && tpY < 5)
                {
                    if (updateArrowCounter)
                    {           // ha jó helyre tettük a nyilat de azt az eredeti helyéről is vettük el, akkor a számláló csökken 
                        ArrowCounterScript.Instance.UpdateArrowCounterSelector(this.gameObject.tag, false);
                    }

                    wrongArrowRuntimeLocation = Utils.RoundXYPos(tpX, tpY);
                    player.wrongLocations.Add(wrongArrowRuntimeLocation);

                    this.transform.position = new Vector2(roundedPos.x + 0.5f, roundedPos.y + 0.5f);
                    this.transform.localScale = new Vector2(0.5f, 0.5f);
                    boxCollider.size = new Vector2(2f, 2f);

                    spawnEffectPS.transform.position = this.transform.position;
                    spawnEffectPS.Play();
                }
                else
                {
                    if (!updateArrowCounter)
                    {                                       // ha rossz helyen van és nem is az eredeti helyén van a nyíl, akkor növeljük a számlálót
                        ArrowCounterScript.Instance.UpdateArrowCounterSelector(this.gameObject.tag, true);   // ArrowCounter script függvénye, leadom neki a tag-et és hony növelje-e a számlálót vagy csökkentse
                        initialScale();                                              // és ez elapján kiválasztja, hogy melyiket kell növelni, ha true, akkor nő a counter, ha false akkor csökken
                    }
                    arrowSpriteRenderer.enabled = true;
                    this.transform.position = originalPosition;
                }
            }
        }
        #endregion

        // Use this for initialization
        private void Start()
        {
            wrongArrowRuntimeLocation = new Vector2();

            mainCamera = CameraInstance.Instance.GetComponent<Camera>();
            player = Player.Instance;
            tileMap = GameObject.Find("Tilemap").GetComponent<Tilemap>();
            wrongPositionObject = GameObject.Find("WrongPositionPrefab");
            rightPositionObject = GameObject.Find("RightPositionPrefab");

            arrowSpriteRenderer = this.GetComponent<SpriteRenderer>();
            shadowSpriteRenderer = this.transform.Find("Shadow").GetComponent<SpriteRenderer>();
            shadowSpriteRenderer.enabled = false;

            spawnEffectPS = transform.parent.Find("ArrowSpawnEffect").GetComponent<ParticleSystem>();
            boxCollider = gameObject.GetComponent<BoxCollider2D>();

            originalTag = this.tag;
            originalPosition = this.transform.position;

            //staticArrowPos();
        }

        private bool ShowWrongLocation(int x, int y)
        {
            int index = isGoodLocations(x, y);

            if (index == 1)
            {
                wrongPositionObject.transform.position = new Vector2(100, 100);
                arrowSpriteRenderer.enabled = false;
                shadowSpriteRenderer.enabled = false;
                rightPositionObject.transform.position = new Vector2(x + 0.5f, y + 0.5f);
                return false;
            }
            else if (index == 2)
            {
                rightPositionObject.transform.position = new Vector2(100, 100);
                arrowSpriteRenderer.enabled = false;
                shadowSpriteRenderer.enabled = false;
                wrongPositionObject.transform.position = new Vector2(x + 0.5f, y + 0.5f);
                return false;
            }
            else
            {
                wrongPositionObject.transform.position = new Vector2(100, 100);
                rightPositionObject.transform.position = new Vector2(100, 100);
                arrowSpriteRenderer.enabled = true;
                shadowSpriteRenderer.enabled = true;
                return true;
            }
        }

        private int isGoodLocations(int x, int y)
        {
            for (int i = 0; i < 4; i++)
            {
                if (x == player.roundedInitArrowLocations[i].x && y == player.roundedInitArrowLocations[i].y)
                {
                    return 1;       // mouse/touch is over arrowPos
                }
            }
            int wlCount = player.wrongLocations.Count;
            for (int i = 0; i < wlCount; i++)
            {
                if (x == player.wrongLocations[i].x && y == player.wrongLocations[i].y)
                {
                    return 2;       // is over a wrong location
                }
            }

            return 3;               // it's fine
        }

        private Vector2 getMousePosition()
        {
#if UNITY_EDITOR
            return mainCamera.ScreenToWorldPoint(Input.mousePosition);
#else
            return mainCamera.ScreenToWorldPoint(Input.GetTouch(0).position);
#endif
        }


        private void initialScale()
        {
            this.transform.localScale = new Vector2(.72f, .72f);
            boxCollider.size = new Vector2(1.333f, 1.333f);
        }

        private void staticArrowPos()
        {
            for (int i = 0; i < 4; i++)
            {
                player.roundedInitArrowLocations[i] = new Vector2(-1.5f + i, -4.5f);
            }
        }

        private bool checkArrowLocation()
        {          // ő nézi meg, hogy kezdő pozicíó-e a nyíl helye
            Vector2 tp = this.transform.position;
            for (int i = 0; i < 4; i++)
            {
                if (player.initialArrowLocations[i] == tp)
                {
                    return true;
                }
            }
            return false;
        }
    }
}