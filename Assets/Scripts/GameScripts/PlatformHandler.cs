﻿using Character;
using GameComponent;
using UnityEngine;

public class PlatformHandler : MonoBehaviour
{
    private void Awake()
    {
        GameObject magnifyGlass = new GameObject("MagnifyGlass");
        magnifyGlass.transform.parent = GameObject.Find("GameContainer").transform;
#if UNITY_EDITOR
        GameObject.Find("Player").AddComponent<DirectionPanel>();
        magnifyGlass.AddComponent<MagnifyGlass>();
#else
        //magnifyGlass.transform.position = new Vector3(0, 0, -10);
        GameObject.Find("Player").AddComponent<DirectionTouchPanel>();
        magnifyGlass.AddComponent<MagnifyTouchGlass>();
#endif
    }
}
