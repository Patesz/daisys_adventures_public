﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManagerScript : MonoBehaviour {

    public static GameManagerScript Instance { get; private set; }
    public static float gameSpeed = 0f;

    private List<Level> levels;
    /*private int numberOfLevel;
    public int upArrowCount { get; private set; }
    public int downArrowCount { get; private set; }
    public int leftArrowCount { get; private set; }
    public int rightArrowCount { get; private set; }
                                            //      light green     dark green
    public Color32 magnifyglassBgColor;     // 78, 159, 6, 255 / 40, 117, 44, 255*/

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            Application.targetFrameRate = -1;
            DontDestroyOnLoad(gameObject);
        } else
        {
            Destroy(gameObject);
        }
        levels = new List<Level>();
        LoadLevels();
    }

    private void LoadLevels()
    {
        int numberOfLevel = SceneManager.sceneCountInBuildSettings - 2;             // -2 because of MainMenu and LevelSelector
        //numberOfLevel = System.IO.Directory.GetFiles("Assets/Resources/Levels").Length/2;     // Resources.FindObjectOfTypeAll only returns ACTIVE objects
        Debug.Log("Number of levels:" + numberOfLevel);
        
        for(int i=0; i<numberOfLevel; i++){
            levels.Add(Resources.Load<Level>("Levels/Level" + (i+1).ToString()));
        }
    }

    public Level getLevel(string currentLevel){
        int levelNum = getLevelNumFromLevelName(currentLevel);
        return levels[levelNum-1];
    }

    public Color32 getLevelBgColor(string currentLevel)
    {
        int levelNum = getLevelNumFromLevelName(currentLevel);
        return levels[levelNum - 1].bgColor;
    }

    public int getLevelNumFromLevelName(string currentLevel)
    {
        char level = currentLevel[currentLevel.Length - 1];
        return (int)char.GetNumericValue(level);
    }
}
