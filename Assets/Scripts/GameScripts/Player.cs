﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Tilemaps;
using TMPro;
using Arrow;

namespace Character {
    public class Player : MonoBehaviour
    {
        public static Player Instance { get; set; }

        private bool isArrowsFade = false;

        [HideInInspector]
        public Animator anim;
        private Animator shakeAnim;

        private readonly string[] waterDeaths = { "I'm drowning!", "Help!", "I can't swim!", "I'm too young to die!" };
        private readonly string[] treeDeaths = { "Outch!", "My head aches!", "Can't go that way!", "My poor head." };

        private GameObject startDaisyBtn;
        private WinLevel winLevelScript;
        public int Points { get; private set; }

        public float speed = 1f;

        public bool moveUp { get; private set; }
        public bool moveDown { get; private set; }
        public bool moveLeft { get; private set; }
        public bool moveRight { get; private set; }

        public static bool go { get; set; }

        public Tilemap tilemap { get; private set; }
        public List<Vector2> waterTilePositions { get; private set; }
        public List<Vector2> wrongLocations { get; set; }
        public Vector2[] initialArrowLocations { get; private set; }
        public Vector2[] roundedInitArrowLocations { get; private set; }
        public Vector2[] collectableLocations { get; private set; }
        public List<Vector2> treeLocations { get; private set; }

        private TextMeshProUGUI text;
        private Transform character;
        private GameObject deathFeedback;
        private GameObject[] collectableObj;
        private GameObject[] treeObj;
        private GameObject cofferObj;
        public float targetXPos { get; set; }
        public float targetYPos { get; set; }

        public Vector2 playerStartPos { get; private set; }

        public List<Transform> uArrowObj { get; private set; }
        public List<Transform> dArrowObj { get; private set; }
        public List<Transform> lArrowObj { get; private set; }
        public List<Transform> rArrowObj { get; private set; }

        private GameObject arrows;
        private RuntimeAnimatorController arrowController;
        private GameObject arrowHolder;

        #region Initialize
        private void Awake()
        {
            Instance = this;

            initialArrowLocations = new Vector2[4];
            collectableLocations = new Vector2[3];
            roundedInitArrowLocations = new Vector2[4];

            wrongLocations = new List<Vector2>();
            waterTilePositions = new List<Vector2>();
            treeLocations = new List<Vector2>();

            playerStartPos = new Vector2();

            uArrowObj = new List<Transform>();
            dArrowObj = new List<Transform>();
            lArrowObj = new List<Transform>();
            rArrowObj = new List<Transform>();
        }

        private bool checkIfWater(char name)
        {
            if (name >= '0' && name <= '9')
            {
                return true;
            }
            return false;
        }

        private void Start()
        {
            go = false;
            playerStartPos = this.transform.position;

            shakeAnim = CameraInstance.Instance.GetComponent<Animator>();
            anim = this.GetComponentInChildren<Animator>();

            deathFeedback = GameObject.Find("DeathFeedback");
            text = deathFeedback.GetComponent<TextMeshProUGUI>();

            character = gameObject.transform.Find("Character");

            startDaisyBtn = GameObject.Find("StartDaisy_btn");
            winLevelScript = GameObject.Find("WinPanel").GetComponent<WinLevel>();
            tilemap = GameObject.Find("Tilemap").GetComponent<Tilemap>();

            treeObj = GameObject.FindGameObjectsWithTag("GreenTree");
            collectableObj = GameObject.FindGameObjectsWithTag("Collectable");
            cofferObj = GameObject.FindGameObjectWithTag("FinishObj");

            arrowHolder = GameObject.Find("ArrowHolder");
            arrows = GameObject.Find("Arrows");
            arrowController = arrows.GetComponent<Animator>().runtimeAnimatorController;

            for (int i = 0; i < 4; i++)
            {
                addInitialArrowPosition(arrows.FindChildWithTag(Utils.direction[i] + "Arrow"), i);
            }

            targetXPos = playerStartPos.x; targetYPos = playerStartPos.y;
            setMoveRightTrue();
            rotateToRight();

            wrongLocations.Add(Utils.RoundPosition(playerStartPos));

            foreach (var pos in tilemap.cellBounds.allPositionsWithin)
            {
                Vector3Int localPlace = new Vector3Int(pos.x, pos.y, pos.z);
                if (tilemap.HasTile(localPlace) && checkIfWater(tilemap.GetTile(localPlace).ToString()[0]))            // If tile first element is a number
                {
                    Vector2 waterPos = Utils.RoundPosition((Vector2Int)localPlace);
                    waterTilePositions.Add(waterPos);
                    wrongLocations.Add(waterPos);
                }
            }

            for (int i = 0; i < collectableObj.Length; i++)
            {
                Vector2 pos = Utils.RoundPosition(collectableObj[i].transform.position);

                wrongLocations.Add(pos);
                collectableLocations[i] = pos;
            }

            wrongLocations.Add(Utils.RoundPosition(cofferObj.transform.position));

            for (int i = 0; i < treeObj.Length; i++)
            {
                Vector2 pos = Utils.RoundPosition(treeObj[i].transform.position);

                treeLocations.Add(pos);
                wrongLocations.Add(pos);
            }
        }

        private void addInitialArrowPosition(Transform arrow, int i)
        {
            initialArrowLocations[i] = arrow.position;            // ide állítom vissza a nyilakat restart-nál

            roundedInitArrowLocations[i] = Utils.RoundPosition(arrow.position);       // kerekítetten meg azért kell ha az a szerencsétlen arra jár akkor csak így érzékeli hogy ütközik vele és kezdje újra a pályát mert rossz helyen van
        }
        #endregion

        #region Player 

        private void Move()
        {
            Vector2 targetPos = Vector2.MoveTowards(this.transform.position, new Vector2(targetXPos, targetYPos), speed * Time.deltaTime);
            this.transform.position = targetPos;
        }

        private void Update()
        {
            if (go)
            {
                Move();
                playerWalkingOnArrowPosition();

                if (this.transform.position.x == targetXPos && this.transform.position.y == targetYPos)
                {
                    Vector2 roundedPlayerPos = Utils.RoundXYPos(targetXPos, targetYPos);

                    playerWalkingOnWater(roundedPlayerPos);
                    playerWalkingOutside(roundedPlayerPos);
                    playerWalkingToTree(roundedPlayerPos);

                    if (wrongLocations.Contains(roundedPlayerPos))
                    {
                        wrongLocations.Remove(roundedPlayerPos);
                    }

                    if (moveUp)
                    {
                        targetYPos += 1;
                    }
                    else if (moveDown)
                    {
                        targetYPos -= 1;
                    }
                    else if (moveRight)
                    {
                        targetXPos += 1;
                    }
                    else
                    {
                        targetXPos -= 1;
                    }

                    wrongLocations.Add(Utils.RoundXYPos(targetXPos, targetYPos));
                }
            }
        }


        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (go)
            {
                if (ArrowCounterScript.Instance.arrowCount > 0 && !isArrowsFade)
                {
                    Debug.Log("It is possible to collide with an arrow! Arrow count: " + ArrowCounterScript.Instance.arrowCount);

                    if (collision.gameObject.tag.Contains("Arrow"))
                    {
                        Animator arrowAnim = collision.gameObject.AddComponent<Animator>();
                        arrowAnim.runtimeAnimatorController = arrowController;
                        arrowAnim.SetTrigger("Collect");
                        ArrowCounterScript.Instance.arrowCount--;
                        deleteFromWrongLoc(collision.gameObject.transform.position);
                    }

                    if (collision.gameObject.CompareTag("UpArrow"))
                    {
                        setMoveUpTrue();
                        setUpAnim();
                    }

                    else if (collision.gameObject.CompareTag("RightArrow"))
                    {
                        setMoveRightTrue();
                        setRightAnim();
                    }

                    else if (collision.gameObject.CompareTag("DownArrow"))
                    {
                        setMoveDownTrue();
                        setDownAnim();
                    }

                    else if (collision.gameObject.CompareTag("LeftArrow"))
                    {
                        setMoveLeftTrue();
                        setLeftAnim();
                    }
                }

                if (collision.gameObject.CompareTag("FinishObj"))
                {
                    go = false;
                    winLevelScript.WinThisLevel();
                    Destroy(this.anim);
                    Destroy(deathFeedback);
                    Destroy(this);
                }

                else if (collision.gameObject.CompareTag("Collectable"))
                {
                    collision.gameObject.GetComponent<Animator>().SetTrigger("Collect");
                    Points++;
                    deleteFromWrongLoc(collision.gameObject.transform.position);
                    Destroy(collision.gameObject, 1f);
                }
            }
        }
        private void deleteFromWrongLoc(Vector2 toDelete)
        {
            Vector2 roundedDel = Utils.RoundPosition(toDelete);

            if (wrongLocations.Contains(roundedDel))
            {
                wrongLocations.Remove(roundedDel);
            }
        }

        private void playerWalkingOnWater(Vector2 roundedPlayerPosition)
        {
            int wPosLen = waterTilePositions.Count;
            for (int i = 0; i < wPosLen; i++)
            {
                if (roundedPlayerPosition == waterTilePositions[i])
                {
                    int index = Random.Range(0, waterDeaths.Length);
                    anim.SetTrigger("Drown");
                    showDeathFeedback(waterDeaths[index]);
                    return;
                }
            }

        }

        private void playerWalkingOutside(Vector2 playerPosition)
        {
            if (playerPosition.x > 10 || playerPosition.x < -10 || playerPosition.y < -5 || playerPosition.y > 5)
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }

        }

        private void playerWalkingToTree(Vector2 roundedPlayerPosition)
        {
            for (int i = 0; i < treeObj.Length; i++)
            {
                if (roundedPlayerPosition.x == treeLocations[i].x && roundedPlayerPosition.y == treeLocations[i].y)
                {
                    int index = Random.Range(0, treeDeaths.Length);
                    shakeAnim.SetBool("shake", true);
                    ParticleSystem ps = treeObj[i].GetComponent<ParticleSystem>();
                    if (ps != null)
                    {
                        var emission = ps.emission;
                        var main = ps.main;

                        main.gravityModifier = 20f;
                        emission.rateOverTime = 120f;
                    }
                    showDeathFeedback(treeDeaths[index]);
                    return;
                }
            }
        }
        #endregion

        #region PlayerOnArrows
        // Ha a kezdő nyilakon akarna sétálgatni...
        private void playerWalkingOnArrowPosition()
        {
            if (targetXPos > -2 && targetXPos < 2.5 && targetYPos <= -4.25)
            {
                if (!isArrowsFade)
                {
                    fadeArrow(130);
                    fadeArrowHolder(new Color32(255, 255, 255, 100));
                    isArrowsFade = true;
                }
                return;
            }
            if (isArrowsFade)
            {
                unFadeArrows();
                fadeArrowHolder(new Color32(255, 255, 255, 255));
                isArrowsFade = false;
            }
        }

        private void fadeArrow(byte alpha)
        {
            for (int i = 0; i < 4; i++)
            {
                GameObject[] arrow = GameObject.FindGameObjectsWithTag(Utils.direction[i] + "Arrow");

                bool fadeAtLeastOnce = false;
                for (int j = 0; j < arrow.Length; j++)
                {
                    if (isArrowOnInitPos(arrow[j]))
                    {
                        SpriteRenderer sp = arrow[j].GetComponent<SpriteRenderer>();
                        if (j == 0 || !fadeAtLeastOnce)
                        {
                            sp.color = new Color32(204, 53, 121, alpha);
                            fadeAtLeastOnce = true;
                        }
                        else
                        {
                            sp.color = new Color32(204, 53, 121, 0);
                        }
                    }
                }
            }
        }

        private bool isArrowOnInitPos(GameObject arrow)
        {
            for (int i = 0; i < 4; i++)
            {
                if ((Vector2)arrow.transform.position == initialArrowLocations[i])
                {
                    return true;
                }
            }
            return false;
        }

        private void fadeArrows(byte alpha)
        {
            SpriteRenderer[] container = arrows.transform.GetComponentsInChildren<SpriteRenderer>();
            for (int i = 0; i < container.Length; i += 2)
            {
                Vector2 roundedArrowPos = Utils.RoundPosition(container[i].transform.position);
                if (i < 8) {
                    for (int j = 0; j < 4; j++)
                    {
                        if (roundedArrowPos == roundedInitArrowLocations[j])
                        {
                            container[i].color = new Color32(204, 53, 121, alpha);
                        }
                    }
                } else {
                    for (int j = 0; j < 4; j++)
                    {
                        if (roundedArrowPos == roundedInitArrowLocations[j])
                        {
                            container[i].enabled = false;
                        }
                    }
                }
            }
        }

        private void unFadeArrows()
        {
            SpriteRenderer[] container = arrows.transform.GetComponentsInChildren<SpriteRenderer>();
            int len = container.Length;
            for (int i = 0; i < len; i += 2)
            {
                Color col = container[i].color;
                container[i].color = new Color(col.r, col.g, col.b, 1);
            }
        }

        private void fadeArrowHolder(Color32 color)
        {
            SpriteRenderer[] container = arrowHolder.transform.GetComponentsInChildren<SpriteRenderer>();
            int len = container.Length;
            for (int i = 0; i < len; i++)
            {
                container[i].color = color;
            }
        }
        #endregion

        #region TriggerAnim
        public void setIdle()
        {
            anim.SetTrigger("GoIdle");
        }
        public void setRightAnim()
        {
            anim.SetTrigger("GoRight");
        }
        public void setLeftAnim()
        {
            anim.SetTrigger("GoLeft");
        }
        public void setUpAnim()
        {
            anim.SetTrigger("GoUp");
        }
        public void setDownAnim()
        {
            anim.SetTrigger("GoDown");
        }
        #endregion

        #region SetDirections
        public void setAllDirectionFalse()
        {
            moveUp = false;
            moveDown = false;
            moveRight = false;
            moveLeft = false;
        }

        public void setMoveRightTrue()
        {
            moveRight = true;
            moveDown = false;
            moveUp = false;
            moveLeft = false;
        }

        public void rotateToRight()
        {
            if (character.transform.rotation.y != 180)
            {
                character.transform.eulerAngles = new Vector3(0, 180, 0);
            }
        }

        public void setMoveLeftTrue()
        {
            moveLeft = true;
            moveRight = false;
            moveDown = false;
            moveUp = false;
        }

        public void setMoveUpTrue()
        {
            moveLeft = false;
            moveRight = false;
            moveDown = false;
            moveUp = true;
        }

        public void setMoveDownTrue()
        {
            moveDown = true;
            moveUp = false;
            moveLeft = false;
            moveRight = false;
        }
        #endregion

        public void showDeathFeedback(string deathCause)
        {
            go = false;                         // stops Update function
            deathFeedback.transform.position = new Vector2(targetXPos, targetYPos + 0.4f);
            text.text = deathCause;
            StartCoroutine(showDuration(2f));
        }

        private IEnumerator showDuration(float seconds)
        {
            deathFeedback.SetActive(true);
            yield return new WaitForSeconds(seconds);
            shakeAnim.SetBool("shake", false);
            deathFeedback.SetActive(false);
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

        /* #region Reset
         private void resetLevelToInitialState()
        {   
            isBoxCollide = false;

            setIdle();                                  // set idle animation                      
            anim.speed = 1f;                            // start playing animation with normal speed
            this.transform.position = playerStartPos;   // move player to initial position            // this.transform.position = GameManagerScript.Instance.getLevels()[GameManagerScript.Instance.LevelNames.IndexOf(SceneManager.GetActiveScene().name)].playerStartingPosition;
            targetPosition = playerStartPos;           // reset currentPos (it prevents abnormal movement)

            setMoveRightTrue();
            rotateToRight();
            startDaisyBtnScript.setToggleButtonCounterZero();
            resetAllCollectable();
            resetAllArrowPositionAndSize();
            arrowCounterScript.initialArrowCounterState();
            arrowCounterScript.arrowCount = 0;
            resetWrongLocations();
            resetBoxes();
        }

        private void resetWrongLocations()
        {
            if (wrongLocations.Count > wrongLocationStartingSize)
            {
                wrongLocations.RemoveRange(wrongLocationStartingSize, wrongLocations.Count - wrongLocationStartingSize);
            }
        }

        private void resetAllCollectable()
        {
            Points = 0;
            for(int i=0; i<collectableObj.Length; i++){
                collectableObj[i].SetActive(true);
                Vector2 collPos = new Vector2(Mathf.Floor(collectableObj[i].transform.position.x), Mathf.Floor(collectableObj[i].transform.position.y));
                if(!wrongLocations.Contains(collPos)){
                    wrongLocations.Add(collPos);
                }
            }
        }

        private void resetBoxes(){
            for(int i=0; i<boxObj.Length; i++){
                boxObj[i].transform.position = initialBoxLocations[i];
                wrongLocations.Add(new Vector2(Mathf.Floor(initialBoxLocations[i].x),Mathf.Floor(initialBoxLocations[i].y)));
            }
        }

        private void resetAllArrowPositionAndSize()
        {
            for (int i = 0; i < uArrowObj.Length; i++)
            {
                this.uArrowObj[i].transform.position = this.initialArrowLocations[0];
                this.uArrowObj[i].transform.localScale = new Vector2(arrowSize, arrowSize);
                this.uArrowObj[i].SetActive(true);
            }
            for (int i = 0; i < dArrowObj.Length; i++)
            {
                this.dArrowObj[i].transform.position = this.initialArrowLocations[1];
                this.dArrowObj[i].transform.localScale = new Vector2(arrowSize, arrowSize);
                this.dArrowObj[i].SetActive(true);
            }
            for (int i = 0; i < lArrowObj.Length; i++)
            {
                this.lArrowObj[i].transform.position = this.initialArrowLocations[2];
                this.lArrowObj[i].transform.localScale = new Vector2(arrowSize, arrowSize);
                this.lArrowObj[i].SetActive(true);
            }
            for (int i = 0; i < rArrowObj.Length; i++)
            {
                this.rArrowObj[i].transform.position = this.initialArrowLocations[3];
                this.rArrowObj[i].transform.localScale = new Vector2(arrowSize, arrowSize);
                this.rArrowObj[i].SetActive(true);
            }
        }
        #endregion*/
    }
}