using UnityEngine;

namespace Audio
{
    public class BtnSound : MonoBehaviour
    {
        public void PlayEffect(AudioClip buttonSound)
        {
            SoundManager.Instance.PlayEffect(buttonSound);
        }
    }
}
