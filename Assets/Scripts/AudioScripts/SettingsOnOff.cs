﻿using Audio;
using UnityEngine;

public class SettingsOnOff : MonoBehaviour {
	private string isEffectsEnabled;		// Unfortunately PlayerPrefs have no bool get set method
	private string isMusicEnabled;
	private string isCloudsEnabled;

	private GameObject effOn, effOff;
	private GameObject mscOn, mscOff;
	private GameObject cloOn, cloOff;
	private GameObject settings;
    
	private void Start() {
		isEffectsEnabled = PlayerPrefs.GetString("isEffectsEnabled", "enable");	// returns key string if it already exists, by default it returns "enabled"
		isMusicEnabled = PlayerPrefs.GetString("isMusicEnabled", "enable");
		isCloudsEnabled = PlayerPrefs.GetString("isCloudsEnabled", "enable");

		settings = GameObject.Find("/Canvas/Settings");
		effOn = GameObject.Find("/Canvas/Settings/EffectsOn");
		effOff = GameObject.Find("/Canvas/Settings/EffectsOff");

		mscOn = GameObject.Find("/Canvas/Settings/MusicOn");
		mscOff = GameObject.Find("/Canvas/Settings/MusicOff");

		cloOn = GameObject.Find("/Canvas/Settings/CloudsOn");
		cloOff = GameObject.Find("/Canvas/Settings/CloudsOff");
		
		settings.SetActive(false);

		if(isEffectsEnabled == "enable"){
			effOn.SetActive(true);
			effOff.SetActive(false);
		}
		else{
			effOn.SetActive(false);
			effOff.SetActive(true);
		}

		if(isMusicEnabled == "enable"){
			mscOn.SetActive(true);
			mscOff.SetActive(false);
		}
		else{
			mscOn.SetActive(false);
			mscOff.SetActive(true);
		}

		if(isCloudsEnabled == "enable"){
			cloOn.SetActive(true);
			cloOff.SetActive(false);
		}
		else{
			cloOn.SetActive(false);
			cloOff.SetActive(true);
		}
	}

	public void EffectsOn(){			// disable effectsOn and activate effectsOff image in editor and sets PlayerPref effects to disabled
		PlayerPrefs.SetString("isEffectsEnabled", "disable");
		effOff.SetActive(true);
		effOn.SetActive(false);
	}
	public void EffectsOff(){
		PlayerPrefs.SetString("isEffectsEnabled", "enable");
		effOff.SetActive(false);
		effOn.SetActive(true);
	}

	public void MusicOn(){
		PlayerPrefs.SetString("isMusicEnabled", "disable");
		SoundManager.Instance.PlayMusic(SoundManager.Instance.MusicSource.clip);
		mscOff.SetActive(true);
		mscOn.SetActive(false);
		
	}
	public void MusicOff(){
		PlayerPrefs.SetString("isMusicEnabled", "enable");
		SoundManager.Instance.PlayMusic(SoundManager.Instance.MusicSource.clip);
		mscOff.SetActive(false);
		mscOn.SetActive(true);
	}

	public void CloudOn(){
		PlayerPrefs.SetString("isCloudsEnabled", "disable");
		Debug.Log(PlayerPrefs.GetString("isCloudsEnabled", "enable"));
		cloOff.SetActive(true);
		cloOn.SetActive(false);
	}

	public void CloudOff(){
		PlayerPrefs.SetString("isCloudsEnabled", "enable");
		Debug.Log(PlayerPrefs.GetString("isCloudsEnabled", "enable"));
		cloOff.SetActive(false);
		cloOn.SetActive(true);
	}
}
