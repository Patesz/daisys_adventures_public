﻿using UnityEngine;

namespace Audio
{
    public class SoundManager : MonoBehaviour
    {
        // Audio players components.
        public AudioSource EffectsSource;
        public AudioSource MusicSource;

        // Random pitch adjustment range.
        public float LowPitchRange = .95f;
        public float HighPitchRange = 1.05f;

        // Singleton instance.
        public static SoundManager Instance = null;

        // Initialize the singleton instance.
        private void Awake()
        {
            // If there is not already an instance of SoundManager, set it to this.
            if (Instance == null)
            {
                Instance = this;
            }
            //If an instance already exists, destroy whatever this object is to enforce the singleton.
            else if (Instance != this)
            {
                Destroy(gameObject);
            }

            //Set SoundManager to DontDestroyOnLoad so that it won't be destroyed when reloading our scene.
            DontDestroyOnLoad(gameObject);

            SoundManager.Instance.PlayMusic(MusicSource.clip);      // it will either play the clip or set it to null based on the current PlayerPrefs isMusicEnabled value
        }

        // Play a single clip through the sound effects source.
        public void PlayEffect(AudioClip clip)
        {
            Debug.Log(PlayerPrefs.GetString("isEffectsEnabled"));
            if (PlayerPrefs.GetString("isEffectsEnabled", "enable") == "enable")
            {
                EffectsSource.clip = clip;
                EffectsSource.Play();
            }
        }

        // Play a single clip through the music source.
        public void PlayMusic(AudioClip clip)
        {
            Debug.Log(PlayerPrefs.GetString("isMusicEnabled"));
            if (PlayerPrefs.GetString("isMusicEnabled", "enable") == "enable")
            {
                MusicSource.clip = clip;
                MusicSource.Play();
            }
            else
            {
                MusicSource.Stop();
            }
        }

        // Play a random clip from an array, and randomize the pitch slightly.
        public void RandomSoundEffect(params AudioClip[] clips)
        {
            int randomIndex = Random.Range(0, clips.Length);
            float randomPitch = Random.Range(LowPitchRange, HighPitchRange);

            EffectsSource.pitch = randomPitch;
            EffectsSource.clip = clips[randomIndex];
            EffectsSource.Play();
        }

    }
}