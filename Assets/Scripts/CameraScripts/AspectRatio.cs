﻿using UnityEngine;
using UnityEngine.UI;

public class AspectRatio : MonoBehaviour
{
    float width = Screen.width;
    float height = Screen.height;

    void Awake()
    {
    //#if !UNITY_EDITOR
        Vector2 referenceResolution;
 
        if (width/height >= 1.7)                // 16:9
            referenceResolution = new Vector2(960f, 540f);
        else if (width / height > 1.6)          // 5:3
            referenceResolution = new Vector2(1280f, 768f);
        else if (width / height == 1.6)         // 16:10
            referenceResolution = new Vector2(1280f, 800f);
        else if (width / height >= 1.5)         // 3:2
            referenceResolution = new Vector2(960f, 640f);
        else// 4:3
            referenceResolution = new Vector2(1024, 768f);
        
        GetComponent<CanvasScaler>().referenceResolution = referenceResolution;
    //#endif
    }
}
