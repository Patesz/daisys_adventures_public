using UnityEngine;

public class PinchZoom : MonoBehaviour
{
    Vector3 touchStart;
    public float orthoZoomSpeed = 0.0125f;        // The rate of change of the orthographic size in orthographic mode.

    private float originalOrtographicSize;
    private Camera mainCamera;

    bool isMoveCamera = true;

    private void Start() {
        touchStart = new Vector3();

        mainCamera = this.gameObject.GetComponent<Camera>();
        originalOrtographicSize = mainCamera.orthographicSize;
        mainCamera.orthographicSize = originalOrtographicSize;

        Debug.Log(mainCamera.orthographicSize);
    }

    private void Update()
    {
        if(Input.GetMouseButtonDown(0)){
            touchStart = mainCamera.ScreenToWorldPoint(Input.GetTouch(0).position);
        }

        // If there are two touches on the device...
        if (Input.touchCount == 2)
        {
            // Store both touches.
            Touch touchZero = Input.GetTouch(0);
            Touch touchOne = Input.GetTouch(1);

            // Find the position in the previous frame of each touch.
            Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
            Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

            // Find the magnitude of the vector (the distance) between the touches in each frame.
            float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
            float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

            // Find the difference in the distances between each frame.
            float deltaMagnitudeDiff = touchDeltaMag - prevTouchDeltaMag;

            Zoom(deltaMagnitudeDiff * orthoZoomSpeed);
        }
        else if(Input.touchCount == 1){       
                                                            // runs on mouse drag (global not on object collider drag)
            Vector3 direction = touchStart - mainCamera.ScreenToWorldPoint(Input.GetTouch(0).position);
            Vector2 camPos = new Vector2();
            Vector2 wrongPos = new Vector2();
            float camSize = mainCamera.orthographicSize;

            if(camPos.x > -2 * camSize && camPos.x < 2 * camSize && camPos.y > -1*camSize && camPos.y < 1*camSize){     // only move camera if it fits map position (map size x: from -10 to 10 y: from -5 to 5)
                mainCamera.transform.position += direction;
                Debug.Log("SJAT");    
            }
            else{                                                                                 // if camera would go out of map...
                isMoveCamera = false;
                bool store = false;
                if(!store){
                    wrongPos = camPos;                    // stores wrongPos location immidietly when out of map to know 
                    store = true;
                }

                Debug.Log(wrongPos);

                if(direction.x < wrongPos.x){
                    Debug.Log("Camera can move again...");
                    camPos.x = camPos.x-1f;
                    isMoveCamera = true;
                }
            }
            //Debug.Log(isCameraIsInMap);
            //Debug.Log(camera.transform.position);*/
        }
        

        if (Input.GetAxis("Mouse ScrollWheel") > 0f ){          // forwards
            Zoom(orthoZoomSpeed);
        }
        else if (Input.GetAxis("Mouse ScrollWheel") < 0f ){     // backwards
            Zoom(-orthoZoomSpeed);
        }
        
	}

    private void Zoom(float increment){
        mainCamera.orthographicSize = Mathf.Clamp(mainCamera.orthographicSize - increment, originalOrtographicSize/2, originalOrtographicSize);
    }

}