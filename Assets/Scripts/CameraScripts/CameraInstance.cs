﻿using UnityEngine;

public class CameraInstance : MonoBehaviour {
	public static CameraInstance Instance { get; private set; }

	// Initialize the singleton instance.
	private void Awake()
	{
		if (Instance == null)
		{
			Instance = this;
        }
		else
		{
			Destroy(gameObject);
		}
    }
    /*private void Start()
    {
        GameObject canvas = GameObject.Find("Canvas");
        Canvas c = canvas.GetComponent<Canvas>();
        c.renderMode = RenderMode.ScreenSpaceCamera;
        c.worldCamera = gameObject.GetComponent<Camera>();
    }*/
}
