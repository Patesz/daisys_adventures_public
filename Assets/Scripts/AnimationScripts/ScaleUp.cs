﻿using UnityEngine;

public class ScaleUp : MonoBehaviour
{
    private void Awake()
    {
        this.GetComponent<RectTransform>().localScale = new Vector3(2f,2f,1f);
    }
}
