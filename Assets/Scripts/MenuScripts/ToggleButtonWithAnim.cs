﻿using UnityEngine;

namespace ButtonNp
{
    public class ToggleButtonWithAnim : MonoBehaviour
    {
        public Animator toAnimate;
        public GameObject panel;
        
        private int counter = 0;

        public void ToggleButton()
        {
            // Open
            if (counter % 2 == 0)
            {
                panel.SetActive(true);
                toAnimate.SetTrigger("Open");
            }
            // Close
            else
            {
                panel.SetActive(false);
                toAnimate.SetTrigger("Close");
            }
            this.counter++;
        }
    }
}
