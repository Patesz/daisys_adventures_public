using Arrow;
using GameComponent;
using UnityEngine;

namespace ButtonNp
{
    public class ToggleButton : MonoBehaviour
    {
        private GameObject startDaisyBtn;
        private MagnifyGlass magnifyGlassScript;

        [HideInInspector]
        public uint counter = 0;

        private void Start()
        {
            startDaisyBtn = GameObject.Find("StartDaisy_btn");
            magnifyGlassScript = GameObject.Find("MagnifyGlass").GetComponent<MagnifyGlass>();
        }

        public void PauseToggle(GameObject panel)
        {
            if (counter % 2 == 1)
            {
                if (!panel.activeInHierarchy)
                {
                    startDaisyBtn.SetActive(false);
                    panel.SetActive(true);
                    this.counter++;
                }
                else
                {                                       //when pause panel active
                    startDaisyBtn.SetActive(true);
                    panel.SetActive(false);
                    Time.timeScale = GameManagerScript.gameSpeed;
                    ArrowManager.Instance.ActivateArrowScript(true);
                }
            }
            else
            {
                startDaisyBtn.SetActive(false);
                panel.SetActive(true);
                GameManagerScript.gameSpeed = Time.timeScale;
                Time.timeScale = 0f;
                ArrowManager.Instance.ActivateArrowScript(false);
            }
            this.counter++;
        }
        public void ActivateStartBtn()
        {
            this.counter++;
            startDaisyBtn.SetActive(true);
            Time.timeScale = GameManagerScript.gameSpeed;
            ArrowManager.Instance.ActivateArrowScript(true);
        }
        public void SetTimeScale(float speed)
        {
            Time.timeScale = speed;
        }

    }
}