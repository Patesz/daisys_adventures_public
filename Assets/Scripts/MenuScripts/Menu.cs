﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace MenuNp
{
    public class Menu : MonoBehaviour
    {
        public void LoadLevelSelector()
        {
            SceneManager.LoadScene("Level Selector");
        }

        public void LoadMainMenu()
        {
            SceneManager.LoadScene("Main Menu");
        }

        public void Restart()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

        public void NextLevel()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }

        public void SetTimeScale(float speed)
        {
            Time.timeScale = speed;
        }

        //terminates application (only in built game)
        public void Quit()
        {
            Application.Quit();
        }
    }
}
