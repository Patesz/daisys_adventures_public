﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

namespace MenuNp
{
    public class LevelSelect : MonoBehaviour {
        // !!! REQUIRES CONFIGURATION IF LEVEL COUNT NOT EQUALS WITH 10 !!!
        private readonly int levelCount = 10;
        // !!!

        private static int levelBeaten;

        private void FindLocksAndAnimator() {

            //Level locks are starts from 2 and should unlock level to the beaten level+1
            for (int i = 1; i < levelBeaten + 1; i++) {
                Destroy(GameObject.Find("level_lock_" + (i+1)));
            }

            Animator newLevelAnim = GameObject.Find("Level" + (levelBeaten + 1).ToString() + "_btn").AddComponent<Animator>();
            newLevelAnim.runtimeAnimatorController = GameObject.Find("Animator").GetComponent<Animator>().runtimeAnimatorController;

            newLevelAnim.SetTrigger("start");
        }

        private void DestroyLevels() {
            for (int i = levelBeaten + 2; i <= levelCount; i++)
            {
                GameObject d = GameObject.Find("level_" + i);       // DOES NOT WORK WITH TRANSFORM (CANT DESTROY)
                Destroy(d);
            }
        }

        private void EnableStars() {
            const string level = "Level";
            int starCounter = 0;

            for (int i = 0; i <= levelBeaten; i++) {
                string lvl = level + (i+1).ToString();
                int starNum = PlayerPrefs.GetInt(lvl, 0);
                starCounter += starNum;
                for (int j = 0; j < starNum; j++) {
                    RawImage star = GameObject.Find(lvl + "_s" + (j+1).ToString()).GetComponent<RawImage>();
                    star.enabled = true;
                }
            }
            PlayerPrefs.SetInt("stars", starCounter);
            Debug.Log("Earned stars: " + starCounter);
        }

        private void Start()
        {
            levelBeaten = PlayerPrefs.GetInt("levelReached", 0);

            EnableStars();
            Debug.Log("Collect at least " + levelCount * 1.5 + " stars to unlock next stage and beat all the levels");
            if ((levelBeaten % levelCount != 0 || levelBeaten == 0) || PlayerPrefs.GetInt("stars") <= (levelBeaten*1.5))
            {
                Destroy(GameObject.Find("NextStage"));
            }

            FindLocksAndAnimator();
            DestroyLevels();
            PointStatus();
        }

        //in level select scene loads the selected level by it's input field
        public void LoadLevel(string level)
        {
            SceneManager.LoadScene(level);
        }

        public void LoadCurrentLevel() {
            SceneManager.LoadScene("Level" + (levelBeaten + 1).ToString());
        }

        private void PointStatus()
        {
            TextMeshProUGUI status = GameObject.Find("StarCounter").GetComponent<TextMeshProUGUI>();
            int collectedStars = PlayerPrefs.GetInt("stars");

            string text = levelCount * 3 + " / " + collectedStars;
            status.text = text;
        }

    }
}
  