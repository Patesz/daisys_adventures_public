﻿using Character;
using UnityEngine;
using UnityEngine.UI;

namespace ButtonNp
{
    public class SpeedButton : MonoBehaviour
    {
        private Transform character;

        private bool speedUp = false;
        private Player player;
        private RawImage image;

        private Texture imagePlayTexture;
        private Texture imageSpeedTexture;

        private bool firstClick = true;

        private void Start()
        {
            player = Player.Instance;
            character = player.transform.Find("Character");
            image = this.GetComponent<RawImage>();
            imagePlayTexture = Resources.Load("button_play_128") as Texture;
            imageSpeedTexture = Resources.Load("button_speed_128") as Texture;
        }

        public void OnButtonClick()
        {
            if (firstClick)
            {
                Player.go = true;
                firstClick = false;

                if (character.transform.rotation.y != 0)
                {
                    character.transform.eulerAngles = new Vector3(0, 0, 0);
                }
                
                if (player.moveUp && !player.moveDown && !player.moveLeft && !player.moveRight)
                {             // to know when we hit play which animation to play
                    player.setUpAnim();
                }
                else if (!player.moveUp && player.moveDown && !player.moveLeft && !player.moveRight)
                {
                    player.setDownAnim();
                }
                else if (!player.moveUp && !player.moveDown && player.moveLeft && !player.moveRight)
                {
                    player.setLeftAnim();
                }
                else
                {
                    player.setRightAnim();
                }
            }

            speedUp = !speedUp;
            if (speedUp)
            {
                image.texture = imagePlayTexture;
                Time.timeScale = 1f;
            }
            else
            {
                image.texture = imageSpeedTexture;
                Time.timeScale = 2f;
            }
        }
    }
}
