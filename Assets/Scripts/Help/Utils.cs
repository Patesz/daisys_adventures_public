﻿using System.Collections.Generic;
using UnityEngine;

public static class Utils{

    public static char[] dir = { 'U', 'D', 'L', 'R' };
    public static string[] direction = { "Up", "Down", "Left", "Right" };

    public static Vector2 RoundPosition(Vector2 pos){
        return new Vector2(Mathf.Floor(pos.x), Mathf.Floor(pos.y));
    }
    public static Vector2Int RoundV2IntPosition(Vector2 pos)
    {
        return new Vector2Int((int)Mathf.Floor(pos.x), (int)Mathf.Floor(pos.y));
    }
    public static Vector2 RoundXYPos(float x,float y)
    {
        return new Vector2(Mathf.Floor(x), Mathf.Floor(y));
    }


    public static Component FindComponentInChildWithTag<T>(this GameObject parent, string tag){
        Transform t = parent.transform;
        foreach(Transform tr in t){
            if(tr.tag == tag){
                return tr.GetComponent<Component>();
            }
        }
        return null;
    }

    public static List<Transform> FindChildrenWithTag(this GameObject parent, string tag){
        Transform t = parent.transform;
        List<Transform> children = new List<Transform>();
        foreach(Transform tr in t){
            if(tr.tag == tag){ 
                children.Add(tr);
            }
        }
        if(children != null){
            return children;
        }
        else{
            return null;
        }
    }

    public static Transform[] FindAllChildWithTag(this GameObject parent, string tag){
        int i = 0;

        Transform t = parent.transform;
        Transform[] children = null;
        foreach(Transform tr in t){
            if(tr.tag == tag){ 
                children[i] = tr;
                i++;
            }
        }
        if(children != null){
            return children;
        }
        else{
            return null;
        }
    }

    public static Transform FindInHierarchy(this GameObject thisGO, string name){
        Transform parent = thisGO.transform.parent;
        Transform go = parent.transform.Find(name);
        return parent;
    }
     
    // Finds GameObject's child by tag name
    public static Transform FindChildWithTag(this GameObject thisGameObject, string tag)
    {
        Transform t = thisGameObject.transform;
        foreach (Transform tr in t)
        {
            if (tr.tag == tag)
            {
                return tr;
            }
        }
        return null;
    }

}
