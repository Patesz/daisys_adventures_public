﻿using Arrow;
using Character;
using System.Collections;
using UnityEngine;

public class ShowHelp : MonoBehaviour {

    private Camera mainCamera;

    private bool isCoroutineAlreadyRunning = false;

    private SpriteRenderer arrowSpriteRenderer;
    private Player playerScript;
    private GameObject player;

    private Vector2 playerPos;
    private Vector2 roundedPlayerPos;
    private Vector2 cofferPos;
    
    void Start()
    {
        mainCamera = CameraInstance.Instance.GetComponent<Camera>();

        playerScript = Player.Instance;
        player = playerScript.gameObject;
        playerPos = player.transform.position;
        roundedPlayerPos = Utils.RoundPosition(playerPos);

        cofferPos = GameObject.Find("Coffer").transform.position;
        arrowSpriteRenderer = GetComponentInChildren<SpriteRenderer>();

        showPlayer();
    }

    private void showPlayer(){
        this.transform.position = new Vector2(playerPos.x ,playerPos.y + 1);
    }

    private IEnumerator showArrows(float seconds){                              // index must be 0, 1, 2, or 3
        isCoroutineAlreadyRunning = true;
        int i = 0;
        while (ArrowCounterScript.Instance.arrowCount == 0)
        {
            transform.position = new Vector2(playerScript.initialArrowLocations[i].x, playerScript.initialArrowLocations[i].y + 1);
            yield return new WaitForSeconds(seconds);
            i++;
            if (i > 3)
            {
                i = 0;
            }
        }
    }

    private void showPlayButton(){
        this.transform.position = new Vector2(-8.3f,-3.2f);
    }

    private void showCoffer(){
        this.transform.position = new Vector2(cofferPos.x, cofferPos.y + 1f);
    }

    private void setRendererActive(bool isActive){
        arrowSpriteRenderer.enabled = isActive;
    }

    public Vector2 getWorldPosition(Vector2 screenPos)
    {
        Vector2 worldPos = Utils.RoundPosition(mainCamera.ScreenToWorldPoint(screenPos));
       
        return worldPos;
    }

    private bool playerTouched = false;
    private void Update() {
        if (!Player.go && Input.touchCount == 1)
        {
            if (!playerTouched)
            {
                Vector2 roundedTouchPos = getWorldPosition(Input.GetTouch(0).position);

                showPlayer();
                if (roundedTouchPos == roundedPlayerPos)
                {
                    playerTouched = true;
                }
            }
            else if (ArrowCounterScript.Instance.arrowCount == 0)
            {
                if (!isCoroutineAlreadyRunning)
                {
                    StartCoroutine(showArrows(1.5f));
                }
            }
            else
            {
                showPlayButton();
            }
        }
        if(Player.go)
        {
            showCoffer();
            Destroy(this);
        }
    }

}
