**Játék neve:** Daisy's Adventures

**Kategória:** 2D Top-Down Tile game

Unity játékmotor segítségévek fejlesztett játék, verzió: 2019.1.1

Játék leírása: 
    
*   Feladat: A karaktert (hős) elnavigálása a megadott cél objektumhoz.
    
*   A célba való eljutást különböző akadályok nehezítik. Például, ha a hős kimegy a pálya területéről, vízbe fullad, 
      vagy egyes akadályoknak nekimegy. Ilyen esetben a pálya újrakezdődik.
    
*   A hősnek egy indulási irányt is megszabhatunk. (jobbra, balra, előre, hátra) 
    
*   A játékosnak lehetősége van a hőst (limitált számú, adott pályához előírt) irányjelző nyilakkal navigálni melyeket a pálya 
      csempéire (Tile) helyezhet le. A karakter menet közben ezekkel a nyilakkal ütközve a megfelelő irányba fog tovább haladni.
    
*   Minden pályán 3 darab gyűjthető objektum van. Ezek felvételével szerzi a játékos a pontokat.
    
*   A szint sikeres ha a hős eljut a cél objektumig. Emellett egy adott mennyiségű pontszámot kap, attól függően, hogy mennyi gyémántot vett fel.

*   A játékot menü gombok segítségével újra kezdhetjük, szüneteltethetjük, felgyorsíthatjuk a sebességet vagy akár
      vissza mehetünk a pályaválasztó menübe is.


UI menü rendszer:
    
*   Pálya választó interface (Level Selector). Itt választhatjuk ki melyik pályát szeretnénk lejátszani.
    *   Amennyiben még az előző szinteket nem teljesítette a játékos az új szintek nem játszhatóak.
    *   A teljesített pályákhoz az adott mennyiségű csillag jelenik meg attól függően hogy a játékos hány pontot ért el az adott pályán.
    
*   Főoldal (Main Menu):
    *   Itt ki és be kapcsolhatjuk az effekt hangokat és a zenét.
    *   Egyéni animációk.


Egyéni animációk (játékos mozgása, felhők, megszerzendő objektumok, stb), particle effect-ek (fákról leeső levelek, napsugarak, stb.) és egyéb, a játékot dinamikusabbá, játszhatóbbá és élvezhetőbbé
tevő vizuális effektek is megfigyelhetőek.

A játékban megtalálható Tile és Sprite gyűjtemények NEM saját készítésűek. 

A játék verziója jelenleg nem végleges. Funkcionalitás terén szinte kész, viszont csak teszt pályák érhetőek el a játékban.